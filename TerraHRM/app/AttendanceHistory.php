<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceHistory extends Model
{
    protected $table='th_hr_attendance_histories';
    
    public function employee(){
        return $this->belongsTo('App\Employee','emp_id','id');
    }
    
    public function attendance_issue(){
        return $this->hasMany('App\AttendanceIssue','attendance_history_id','id');
    }
}
