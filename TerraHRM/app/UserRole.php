<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    public $primaryKey = 'role_id';
    /**
     * Adding Elequont relationships
     */
    public function permissions(){
        return $this->belongsToMany('App\UserPermission','th_sys_user_role_permissions','role_id','perm_id');
    }
    
    public function users(){
        return $this->hasMany('App\User','role_id','role_id');
    }
}
