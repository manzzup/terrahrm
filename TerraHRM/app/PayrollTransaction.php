<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollTransaction extends Model
{
    protected $table='th_hr_payroll_transactions';
    
    public function employee(){
        return belongsTo('App\Employee','id','emp_id');
    }
    
    public function transaction_type(){
        return belongsTo('App\TransactionType','id','transaction_type_id');
    }
}
