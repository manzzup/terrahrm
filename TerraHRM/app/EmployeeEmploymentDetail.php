<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeEmploymentDetail extends Model
{
    protected $table = 'th_hr_employee_employment_details';
    public function employee(){
        return $this->belongsTo('App\Employee','id','emp_id');
    }
    public function department_obj(){
        return $this->hasOne('App\Department','id','department');
    }
    public function job_category_obj(){
        return $this->hasOne('App\JobCategory','id','job_category');
    }
    public function employment_status_obj(){
        return $this->hasOne('App\EmploymentStatus','employment_status','id');
    }
     public function group_of_companies_obj(){
        return hasOne('App\GroupOfCompanies','group_of_companies','id');
    }
}
