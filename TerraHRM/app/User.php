<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
//Blah
//comment
class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password',
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    
    /**
     * Adding Elequont relationships
     */
    public function role(){
        return $this->hasOne('App\UserRole','role_id','role_id');
    }
    
    public function employee(){
        return $this->hasOne('App\Employee','id','emp_id');
    }
}
