<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRolePermission extends Model
{
    protected $table = 'th_sys_user_role_permissions';
}
