<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::group(['middleware' => ['web']], function () {
       
    
    Route::group(['middleware' => ['auth']], function () {
        
        //dd(Gate::allows('access_admin_module'));
        
        Route::get('/',function(){
            return view('dashboard');
        })->name('dashboard'); 
        Route::get('/dashboard/data','DashboardController@getData')->name('dashboard.data');
        Route::get('/reports/{module}','ReportsController@listReports')->name('report.list');
        Route::get('/reports/view/{rid}','ReportsController@viewReport')->name('report.view');

        
        Route::group(['middleware' => ['role:1']], function () {
            
            Route::group(['middleware' => ['role:2']], function () {
                Route::get('admin/pim/Departments','Admin\PIMController@viewDepartments')->name('admin.pim.viewDepartments');
                Route::get('admin/pim/Departments/dptID={dpt}','Admin\PIMController@removeDepartment')->name('admin.pim.removeDepartment');     
                Route::post('admin/pim/Departments','Admin\PIMController@addDepartment')->name('admin.pim.addDepartment');

                Route::get('admin/pim/GOC','Admin\PIMController@viewGOC')->name('admin.pim.viewGOC');     
                Route::get('admin/pim/GOC/gocID={goc}','Admin\PIMController@removeGOC')->name('admin.pim.removeGOC');     
                Route::post('admin/pim/GOC','Admin\PIMController@addGOC')->name('admin.pim.addGOC');

                Route::get('admin/pim/JobCategories','Admin\PIMController@viewJobCategories')->name('admin.pim.viewJobCategories');     
                Route::get('admin/pim/JobCategories/jobID={job}','Admin\PIMController@removeJobCategories')->name('admin.pim.removeJobCategories');     
                Route::post('admin/pim/JobCategories','Admin\PIMController@addJobCategories')->name('admin.pim.addJobCategories');

                Route::get('admin/pim/EmploymentStatuses','Admin\PIMController@viewEmploymentStatuses')->name('admin.pim.viewEmploymentStatuses');     
                Route::get('admin/pim/EmploymentStatuses/statusID={job}','Admin\PIMController@removeEmploymentStatuses')->name('admin.pim.removeEmploymentStatuses');     
                Route::post('admin/pim/EmploymentStatuses','Admin\PIMController@addEmploymentStatuses')->name('admin.pim.addEmploymentStatuses');
            });
            
            Route::group(['middleware' => ['role:3']], function () {
                Route::get('admin/leave/LeaveTypes','Admin\LeaveController@viewLeaveTypes')->name('admin.leave.viewLeaveTypes');
                Route::get('admin/leave/LeaveTypes/leaveID={lid}','Admin\LeaveController@removeLeaveType')->name('admin.leave.removeLeaveType');
                Route::post('admin/leave/LeaveTypes','Admin\LeaveController@addLeaveType')->name('admin.leave.addLeaveType');

                Route::get('admin/leave/LeavePeriod','Admin\LeaveController@viewLeavePeriod')->name('admin.leave.viewLeavePeriods');
                Route::get('admin/leave/LeavePeriod/$leavePeriod={lid}','Admin\LeaveController@removeLeavePeriod')->name('admin.leave.removeLeavePeriod');
                Route::post('admin/leave/LeavePeriod','Admin\LeaveController@addLeavePeriod')->name('admin.leave.addLeavePeriod');
            });
            
            Route::group(['middleware' => ['role:6']], function () {
                Route::get('admin/user/UserRoles','Admin\UserController@viewUserRoles')->name('admin.user.viewUserRoles');
                Route::get('admin/user/UserRoles/roleID={rid}','Admin\UserController@removeUserRole')->name('admin.user.removeUserRole');
                Route::post('admin/user/UserRoles','Admin\UserController@addUserRole')->name('admin.user.addUserRole');
                
                Route::get('admin/user/Users','Admin\UserController@viewUsers')->name('admin.user.viewUsers');
                Route::post('admin/user/Users','Admin\UserController@addUser')->name('admin.user.addUser');
                Route::get('admin/user/Users/userID={uid}','Admin\UserController@removeUser')->name('admin.user.removeUser');
                
                Route::get('admin/user/AssignPermission','Admin\UserController@viewAssignPermission')->name('admin.user.viewAssignPerm');
                Route::post('admin/user/AssignPermission','Admin\UserController@assignPermission')->name('admin.user.assignPerm');
            });
                
        });
        
        Route::group(['middleware' => ['role:14']], function () {
            Route::get('/pim/test2',function(){
                return view('pim/test2');
            });
            Route::get('/pim/AddEmployee',function(){
                return view('pim/AddEmployee');
            })->name('pim.addEmployee');
            
                        
            Route::group(['middleware' => ['role:17']], function () {
                Route::get('/pim/{emp_id}/ViewPersonalDetails','PIM\EmployeePersonalDetailController@viewPersonalDetails')->name('pim.viewPersonalDetails');
                Route::get('/pim/{emp_id}/ViewEmploymentDetails','PIM\EmployeeEmploymentDetailController@viewEmploymentDetails')->name('pim.viewEmploymentDetails');
                Route::get('/pim/{emp_id}/ViewContactDetails','PIM\EmployeeContactDetailController@viewContactDetails')->name('pim.viewContactDetails');

                Route::post('/pim/{emp_id}/ViewPersonalDetails','PIM\EmployeePersonalDetailController@updatePersonalDetails')->name('pim.updatePersonalDetails');
                Route::post('/pim/{emp_id}/ViewEmploymentDetails','PIM\EmployeeEmploymentDetailController@updateEmploymentDetails')->name('pim.updateEmploymentDetails');
                Route::post('/pim/{emp_id}/ViewContactDetails','PIM\EmployeeContactDetailController@updateContactDetails')->name('pim.updateContactDetails');
            });
            Route::group(['middleware' => ['role:16']], function () {
                Route::post('/pim/AddEmployee','PIM\AddEmployeeController@addEmployee')->name('pim.addEmployee');
            });
            Route::group(['middleware' => ['role:15']], function () {
                Route::get('/pim/ViewEmployee','PIM\SearchEmployeeController@viewSearchEmployees')->name('pim.viewEmployee');
                Route::post('/pim/ViewEmployee','PIM\SearchEmployeeController@searchEmployees')->name('pim.searchEmployees');
            });
            
        });

        
        Route::group(['middleware' => ['role:24']], function () {
            Route::get('/leave',function(){
                return view('leave/test');
            });


            
            
            Route::group(['middleware' => ['role:25']], function () {
                Route::get('leave/ApplyLeave','Leave\AssignLeaveController@viewAssignLeave')->name('leave.apply');
                Route::post('leave/ApplyLeave','Leave\AssignLeaveController@assignLeave')->name('leave.apply');                
            });
            Route::group(['middleware' => ['role:26']], function () {
                Route::get('leave/LeaveSummary','Leave\LeaveHistoryController@viewLeaveHistory')->name('leave.summary');
                Route::post('leave/SearchEmployee','Leave\LeaveHistoryController@searchEmployee')->name('leave.searchEmployee');                
            });
            Route::group(['middleware' => ['role:27']], function () {
                Route::get('leave/ViewEntitlement','Leave\LeaveEntitlementController@viewLeaveEntitlement')->name('leave.entitlement');
                Route::post('leave/AddEntitlement','Leave\LeaveEntitlementController@saveLeaveEntitlement')->name('leave.addEntitlement');         
            });
            Route::group(['middleware' => ['role:28']], function () {            
                Route::get('/leave/LeaveBalance',function(){
                    return view('leave/LeaveBalance');
                })->name('leave.balance');
                Route::post('leave/LeaveBalance','Leave\LeaveEntitlementController@getLeaveBalance')->name('leave.balance');
            });    
            
        });
        
        Route::group(['middleware' => ['role:18']], function () {
            Route::get('/attendance',function(){
                return view('attendance/test');
            });
            
            
            
            Route::group(['middleware' => ['role:19']], function () {
                Route::get('/attendance/UploadAttendance',function(){
                    return view('attendance/UploadAttendance');
                })->name('attendance.upload');
                Route::post('attendance/Upload','Attendance\UploadAttendanceController@uploadFile')->name('attendance.uploadFile');
            });
            Route::group(['middleware' => ['role:20']], function () {
                Route::get('/attendance/AttendanceHistory',function(){
                    return view('attendance/AttendanceHistory');
                })->name('attendance.history');
                Route::post('attendance/SearchHistory','Attendance\AttendanceHistoryController@getAttendanceHistory')->name('attendance.getHistory');                
            });
            Route::group(['middleware' => ['role:21']], function () {
                Route::get('/attendance/AttendanceIssues',function(){
                    return view('attendance/AttendanceIssues');
                })->name('attendance.issues');
                Route::post('attendance/AttendanceIssues','Attendance\AttendanceIssueController@getAttendaceIssue')->name('attendance.getIssues');                
            });
            Route::group(['middleware' => ['role:23']], function () {
                Route::get('/attendance/AddCheckoutTime/{his_id}/{isu_id}','Attendance\AttendanceIssueController@viewAddCheckoutTime')->name('attendance.viewAddCheckout');
                Route::post('attendance/AddCheckoutTime/{his_id}/{isu_id}','Attendance\AttendanceIssueController@addCheckoutTime')->name('attendance.addCheckout');            
            });
            Route::group(['middleware' => ['role:22']], function () {
                Route::get('/attendance/AddCheckinTime/{his_id}/{isu_id}','Attendance\AttendanceIssueController@viewAddCheckinTime')->name('attendance.viewAddCheckin');
                Route::post('attendance/AddCheckinTime/{his_id}/{isu_id}','Attendance\AttendanceIssueController@addCheckinTime')->name('attendance.addCheckin');
            });
        });
    });
    //Route::get('/','Admin\UserController@listUsers');
    
    Route::get('login','Auth\AuthController@getLogin');
    Route::post('login', 'Auth\AuthController@postLogin');
    Route::get('logout', 'Auth\AuthController@getLogout')->name('logout');

    // Registration routes...
//    Route::get('register', 'Auth\AuthController@getRegister');
//    Route::post('register', 'Auth\AuthController@postRegister');
});

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
    ]);