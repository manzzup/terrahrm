<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function getData(){
        $dataset = array();
        
        //pie chart 1
        $res = DB::select("select dept.id,
                                    name,
                                    count(emp.emp_id) as numb_of_emps
                                    from th_sys_departments dept inner join th_hr_employee_employment_details emp
                                    on dept.id = emp.department
                                    group by dept.id");
        $labels = array();
        $data = array();
        for($i=0;$i<count($res);$i++){
            array_push($labels,$res[$i]->name);
            array_push($data,$res[$i]->numb_of_emps);
        }
        
        $dataset['pie1'] = [
                            'data' => $data,
                            'labels' => $labels
        ];
        
        //pie chart 2
        $res = DB::select("select goc.id,
                                    name,
                                    count(emp.emp_id) as numb_of_emps
                                    from th_sys_group_of_companies goc inner join th_hr_employee_employment_details emp
                                    on goc.id = emp.group_of_companies
                                    group by goc.id");
        
        $labels = array();
        $data = array();
        for($i=0;$i<count($res);$i++){
            array_push($labels,$res[$i]->name);
            array_push($data,$res[$i]->numb_of_emps);
        }
        
        $dataset['pie2'] = [
                            'data' => $data,
                            'labels' => $labels
        ];
        
        //pie chart 2
        $res = DB::select("SELECT date(ah.date) AS at_date, 
                                    COUNT(*) AS attendance 
                                    FROM th_hr_attendance_histories ah  
                                    where check_in_time IS NOT NULL OR check_out_time is not null
                                    GROUP BY at_date");
        $labels = array();
        $data = array();
        for($i=0;$i<count($res);$i++){
            array_push($labels,$res[$i]->at_date);
            array_push($data,$res[$i]->attendance);
        }
        
        $dataset['line1'] = [
                            'data' => $data,
                            'labels' => $labels
        ];
        
        return json_encode($dataset);
    }
}
