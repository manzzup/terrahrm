<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class ReportsController extends Controller
{
    public function listReports($module){
        $reports = \App\Report::where('module','=',$module)->get();
        
        if($module === 'pim'){
            return View::make('pim/ViewReports',array('reports' => $reports));
        }else if($module === 'leave'){
            return View::make('leave/ViewReports',array('reports' => $reports));
        }else if($module === 'attendance'){
            return View::make('attendance/ViewReports',array('reports' => $reports));
        }
    }
    
    public function viewReport($rid){
        $report = \App\Report::find($rid);
        $link = "http://192.168.75.150/php-reports/report/html/?report=mysql/".$report->filename.".sql";
        
        return View::make('common/viewReport',array('link' => $link));
    }
}
