<?php

namespace App\Http\Controllers\Leave;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\LeaveApplication;

class LeaveHistoryController extends Controller
{
    public function viewLeaveHistory(){
        $departments = \App\Department::all();
        return View::make('leave/LeaveHistory',
                ['dpts'=>$departments
                ]);
    }
    
    
    public function searchEmployee(Request $request){
        $departments = \App\Department::all();
        $fromDate=$request->input('fromDate');
        $fromDate = strtotime("-1 day", strtotime($fromDate));
        $toDate=$request->input('toDate');

        $toDate = strtotime("+1 day", strtotime($toDate));
        
//      
        $this->validate($request, [
            'fromDate' => 'required|date|before:'.date("Y-m-d", $toDate),
            'toDate' => 'required|date|after:'.date("Y-m-d", $fromDate),
            'empName' => 'required_without:department|alpha',
            'department' => 'required_without:empName',
        

      ]);
        $toDate = date("Y-m-d", $toDate);
        $fromDate = date("Y-m-d", $fromDate);
       
        $department=$request->input('department');
        $empID=$request->input('empID');
                
        
        if ( !empty($empID)){
            $leave_histories = LeaveApplication::whereHas('issue_obj',function($query) use($fromDate,$toDate){
                        $query->whereHas('attendance_history', function($query) use($fromDate,$toDate){
                            $query->whereDate('date','>=',$fromDate)->whereDate('date','<=',$toDate);
                            
                        });
            
                   })->where('emp_id','=',$empID)->get();
                                    
        } else{
        
            $leave_histories = LeaveApplication::whereHas('issue_obj',function($query) use($fromDate,$toDate){
                        $query->whereHas('attendance_history', function($query) use($fromDate,$toDate){
                            $query->whereDate('date','>',$fromDate)->whereDate('date','<',$toDate);
                            
                        });
            
                   })->whereHas('employee', function($query) use($department){
                                                  $query->whereHas('employment_details',function($query) use($department){
                                                      $query->where('department',$department);
                                                  });
                                            })->get();
        
           
       }
      \Session::flash('flash_message','No results found!');
        return View::make('leave/LeaveHistory', array('leave_histories' => $leave_histories),array('dpts'=>$departments));
    
       
       }
    
  
     
}
