<?php

namespace App\Http\Controllers\Leave;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class AssignLeaveController extends Controller
{
    
    public function viewAssignLeave(Request $request){
        $leaveTypes = \App\LeaveType::all();
        return View::make('leave/AssignLeave',array('leaveTypes'=>$leaveTypes));
    }
    public function assignLeave(Request $request){
        $this->validate($request, [
            'empID' => 'required|alpha_num',
            'leaveTypes' => 'required|numeric',
            
        ]);
        
        
        $leaveApplication = new \App\LeaveApplication();
        $leaveApplication->emp_id = $request->input('empID');
        $leaveApplication->leave_type = $request->input('leaveTypes');
        $leaveApplication->issue_id = $request->input('isuID');
        $leaveApplication->issue_date = $request->input('isuID');
        $leaveApplication->comment = $request->input('comment');
        $leaveApplication->save();
        
        $attendanceIssue = \App\AttendanceIssue::find($leaveApplication->issue_id);
        $attendaceIssues = $attendanceIssue->attendance_history->attendance_issue;
        
        foreach ($attendaceIssues as $a) {
           $a->resolve = 1;
           $a->save();
        }
        //$attendanceIssue->resolve=1;
        //$attendanceIssue->save();
        \Session::flash('flash_message','Leave Assigned Successfully.');

        return redirect('leave/ApplyLeave');
        


    }
    
    
}
