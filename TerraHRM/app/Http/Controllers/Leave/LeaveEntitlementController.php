<?php

namespace App\Http\Controllers\Leave;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LeaveEntitlementController extends Controller
{
    public function viewLeaveEntitlement(){
        $departments = \App\Department::all();
        $leaveTypes = \App\LeaveType::all();
        $leavePeriod = \App\LeavePeriod::all();
        //var_dump($leavePeriod);
        //echo $leavePeriod->leave_year;
        return View::make('leave/AddLeaveEntitlement',
                ['dpts'=>$departments,
                'leavePeriod'=>$leavePeriod,
                 'leaveTypes'=>$leaveTypes
                ]);
    }

    public function saveLeaveEntitlement(Request $request){
         $this->validate($request, [
            'empID' => 'required|alpha_num',
            'entitlement' => 'required|integer|min:1',
             'department' => 'required|numeric',
             'leaveTypes' => 'required|numeric',
             'leavePeriod' => 'required|numeric',

        ]);
         

        $leaveEnt = new \App\LeaveEntitlement();
        $leaveEnt->emp_id = $request->input('empID');
        $leaveEnt->department = $request->input('department');
        $leaveEnt->leave_type = $request->input('leaveTypes');
        $leaveEnt->leave_period = $request->input('leavePeriod');
        $leaveEnt->entitlements = $request->input('entitlement');
        $leaveEnt->save();
        \Session::flash('flash_message','Leave Entitlement Added Successfully.');
        return redirect('leave/ViewEntitlement');

    }

    public function getLeaveBalance(Request $request){
        if (($request->input('button1')!==null)){

        $this->validate($request, [
            'empID' => 'required|alpha_num',



        ]);

       $empID=$request->input('empID');
        }
        else{
          $empID=Auth::user()->emp_id;
        }
       $leave_types= \App\LeaveType::all();

       //$res = DB::select(DB::raw("SELECT l.leave_type,count(l.leave_type) as taken,(SELECT O.entitlements FROM th_hr_leave_entitlements O WHERE emp_id = $empID and O.leave_type = l.leave_type) as ent FROM th_hr_leave_applications l WHERE l.emp_id = $empID GROUP BY l.leave_type"));

       $res = DB::select("SELECT o.leave_type,o.entitlements as ent, (SELECT count(l.leave_type) FROM th_hr_leave_applications l WHERE emp_id = $empID and l.leave_type = o.leave_type) as taken
                                                                    FROM th_hr_leave_entitlements o
                                                                    WHERE o.emp_id = $empID");

       foreach ($res as $r){
           $r->leave_balance = $r->ent-$r->taken;
           $g= \App\LeaveType::where('id',$r->leave_type)->first();

           $r->leave_type=($g->leave_title);
       }

      // dd($res);
       //$leave_type = \App\LeaveType::where('id',$leave_t)->get();



//       foreach ($leave_entitlement as $e){
//           $entitlements+= $e->entitlements;
//       }
//
//       $leave_applications = \App\LeaveApplication::where('emp_id',$empID)->get();
//
//       $leaveApplications=$leave_applications->count();
//
//       $balance = $entitlements - $leaveApplications;

       return View::make('leave/LeaveBalance', array('results' => $res));


    }


}
