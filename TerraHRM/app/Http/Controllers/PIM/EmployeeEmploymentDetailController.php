<?php

namespace App\Http\Controllers\PIM;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\EmployeeEmploymentDetail;
use App\JobCategory;
use App\Department;
use App\EmploymentStatus;
use App\GroupOfCompany;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use App\Employee;

class EmployeeEmploymentDetailController extends Controller
{
        public function viewEmploymentDetails(Request $request){
        $emp_id = Route::input('emp_id');
        $employee = Employee::find($emp_id);
        $goc = GroupOfCompany::all();
        $departments=  Department::all();
        $job_categories=JobCategory::all();
        $employment_status= EmploymentStatus::all();


        $employment_detail = EmployeeEmploymentDetail::where('emp_id','=',$emp_id)->first();
        if($employment_detail==null){
            $employment_detail= new \App\EmployeeEmploymentDetail();
        }

        return View::make('pim/ViewEmploymentDetail')->with(array(
                                                                  'employee' => $employee,
                                                                  'goc'=>$goc,
                                                                  'employment_detail' => $employment_detail,
                                                                  'departments'=>$departments,
                                                                  'job_categories'=>$job_categories,
                                                                  'employment_status'=> $employment_status
                                                                   ));
    }
    public function updateEmploymentDetails(Request $request){
        $this->validate($request, [

            'jobTitle' => 'required|regex:/(^[A-Za-z. ]+$)+/',
            'joinedDate' => 'required|date',
            'leftDate' => 'date|after:joinedDate',
            'leavingReason'=>'regex:/(^[A-Za-z.,\' ]+$)+/',
        ]);
        $emp_id = Route::input('emp_id');
        $employee = Employee::find($emp_id);
        $goc = GroupOfCompany::all();

        $departments=  Department::all();
        $job_categories=JobCategory::all();
        $employment_status= EmploymentStatus::all();
        $employment_detail = EmployeeEmploymentDetail::where('emp_id','=',$emp_id)->first();

        if($employment_detail==null){
            $employment_detail= new \App\EmployeeEmploymentDetail();
            $employment_detail->emp_id=$emp_id;
        }

        $employment_detail->department=$request->input('department');
        $employment_detail->employment_status=$request->input('empStatus');
        $employment_detail->group_of_companies=$request->input('goc');
        $employment_detail->hr_active_flag=$request->input('employeeStatus');
        $employment_detail->job_category=$request->input('jobCategory');
       $employment_detail->job_title=$request->input('jobTitle');
       $employment_detail->joined_date=$request->input('joinedDate');
       $employment_detail->leaving_reason=$request->input('leavingReason');
       $employment_detail->left_date=$request->input('leftDate');

            $employment_detail->save();
           return View::make('pim/ViewEmploymentDetail')->with(array(
                                                                    'employee' => $employee,
                                                                    'goc'=>$goc,
                                                                    'employment_detail' => $employment_detail,
                                                                    'departments'=>$departments,
                                                                    'job_categories'=>$job_categories,
                                                                    'employment_status'=> $employment_status
                                                                   ))->withSuccess('Updated successfully!');
    }

}
