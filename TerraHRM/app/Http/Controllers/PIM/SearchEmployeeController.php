<?php

namespace App\Http\Controllers\PIM;

use Illuminate\Http\Request;
use App\Department;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Employee;

class SearchEmployeeController extends Controller
{
      public function searchEmployees(Request $request){
           $this->validate($request, [
                'query' => 'required_without:department'
            ]);
           $data=Employee::all();
           $query = $request->input('query');

           $searchBy = $request->input('searchBy');
           $department=$request->input('department');
           $departments=  Department::all();
           if($searchBy == 1){
               $employees = Employee::where('emp_id','like','%'.$query.'%')->get();


           }
           else if($searchBy == 2){
               $employees = Employee::where('full_name','like','%'.$query.'%')
                                    -> orWhere('first_name','like','%'.$query.'%')
                                    -> orWhere('last_name','like','%'.$query.'%')
                                    ->get();

           }
           else if($searchBy == 3){

               //$employmentDetails = EmployeeEmploymentDetail::where('department','=',$query)->get();
               $employees = Employee::whereHas('employment_details',function($query) use($department){
                                        $query->where('department','=',$department);
               })->get();

           }
                 //var_dump($departments);
              return View::make('pim/SearchEmployee')->with(array(
                                                                  'employees' => $employees,
                                                                   'department'=>$department,
                                                                   'departments'=>$departments,
                                                                   'searchBy'=>$searchBy,
                                                                   'query'=>$query,
                                                                   'data'=>$data,

                                                                   ));

    }
    public function viewSearchEmployees(){
        $departments=  Department::all();
        return View::make('pim/SearchEmployee', array('departments'=>$departments));

    }
}
