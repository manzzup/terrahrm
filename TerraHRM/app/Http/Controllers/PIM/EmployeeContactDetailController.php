<?php

namespace App\Http\Controllers\PIM;

use Illuminate\Http\Request;
use App\EmployeeContactDetail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use App\Employee;

class EmployeeContactDetailController extends Controller
{
    public function viewContactDetails(Request $request){
        $emp_id = Route::input('emp_id');
        $employee = Employee::find($emp_id);
        $contact_detail = EmployeeContactDetail::where('emp_id','=',$emp_id)->first();
        if($contact_detail==null){
            $contact_detail= new \App\EmployeeContactDetail();
        }
        return View::make('pim/ViewContactDetail',array('employee' => $employee),array('contact_detail' => $contact_detail));
    }

    public function updateContactDetails(Request $request){

           $this->validate($request, [
            'line1' => 'required|regex:~(^[A-Za-z0-9.,/\-: ]+$)+~',
            'line2' => 'required|regex:~(^[A-Za-z0-9.,/\-: ]+$)+~',
             'city' => 'required|regex:/(^[A-Za-z0-9.,: ]+$)+/',
             'province' => 'required|regex:/(^[A-Za-z0-9,.:, ]+$)+/',
             'homeNo' => 'numeric',
             'workNo' => 'required|numeric',
             'mobileNo' => 'numeric',
             'workMail'=>'required|email',
             'personalMail'=>'email',
             'name'=>'required|regex:/(^[A-Za-z. ]+$)+/',
             'relationship'=>'required|regex:/(^[A-Za-z\',.\- ]+$)+/',
             'contactNo'=>'required|numeric'


        ]);
            $emp_id = Route::input('emp_id');
            $employee = Employee::find($emp_id);
            $contact_detail = EmployeeContactDetail::where('emp_id','=',$emp_id)->first();
            if($contact_detail==null){
                $contact_detail= new \App\EmployeeContactDetail();
                $contact_detail->emp_id=$emp_id;
            }




            $contact_detail->add_line_1=$request->input('line1');
            $contact_detail->add_line_2=$request->input('line2');
            $contact_detail->city=$request->input('city');
            $contact_detail->province=$request->input('province');
            $contact_detail->home_no=$request->input('homeNo');
            $contact_detail->work_no=$request->input('workNo');
            $contact_detail->mobile_no=$request->input('mobileNo');
            $contact_detail->work_email=$request->input('workMail');
            $contact_detail->personal_email=$request->input('personalMail');
             $contact_detail->emergency_name=$request->input('name');
            $contact_detail->emergency_relationship=$request->input('relationship');
            $contact_detail->emergerncy_contact_no=$request->input('contactNo');
             $contact_detail->save();
             return View::make('pim/ViewContactDetail', array('contact_detail' => $contact_detail),array('employee' => $employee))->withSuccess('Updated successfully!');

     }
}
