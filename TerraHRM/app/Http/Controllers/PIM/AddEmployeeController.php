<?php

namespace App\Http\Controllers\PIM;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
class AddEmployeeController extends Controller
{
       public function addEmployee(Request $request){
 
        $this->validate($request, [
            'firstName' => 'required|regex:/(^[A-Za-z. ]+$)+/',
            'lastName' => 'required|regex:/(^[A-Za-z. ]+$)+/',
            'empId' => 'required|unique:th_hr_employees,emp_id'
        ]);
        
        $employee = new \App\Employee();
        $employee->emp_id = $request->input('empId');
        $employee->first_name = $request->input('firstName');
        $employee->last_name = $request->input('lastName');
        $employee->save();
        
        return redirect()->route('pim.viewPersonalDetails',[$employee->id]);
        
    }
    public function viewAddEmployee(){
        
    }
}
