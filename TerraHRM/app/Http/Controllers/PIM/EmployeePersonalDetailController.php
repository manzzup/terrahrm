<?php

namespace App\Http\Controllers\PIM;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Employee;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

class EmployeePersonalDetailController extends Controller
{
    public function viewPersonalDetails(Request $request){
        $emp_id = Route::input('emp_id');
        $employee = Employee::find($emp_id);
        return View::make('pim/ViewPersonalDetail',array('employee' => $employee));
    }

    public function updatePersonalDetails(Request $request){
         $this->validate($request, [
            'fullName' => 'required|regex:/(^[A-Za-z. ]+$)+/',
            'firstName' => 'required|regex:/(^[A-Za-z. ]+$)+/',
            'lastName' => 'required|regex:/(^[A-Za-z. ]+$)+/',
            'gender' => 'required',
            'dateOfBirth' => 'date|before:'.date("Y-m-d"),
            'nicNo' => 'required|regex:/^[0-9]{9}[v,V]$/',
            'licNo' => 'alpha_num',
            'nationality'=>'alpha'
        ]);


            $emp_id = Route::input('emp_id');
            $employee = Employee::find($emp_id);
            $employee->full_name=$request->input('fullName');
            $employee->first_name=$request->input('firstName');
            $employee->last_name=$request->input('lastName');
            $employee->gender=$request->input('gender');
            $employee->marital_status=$request->input('maritalStatus');
            $employee->date_of_birth=$request->input('dateOfBirth');
            $employee->nic=$request->input('nicNo');
            $employee->license=$request->input('driver\'sLicenseNo');
            $employee->nationality=$request->input('nationality');

            $employee->save();

         return View::make('pim/ViewPersonalDetail', array('employee' => $employee))->withSuccess('Updated successfully!');




    }

}
