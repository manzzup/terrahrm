<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use App\AttendanceHistory;
use App\Http\Requests;
use App\AttendanceIssue;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use Session;
use Redirect;

class AttendanceIssueController extends Controller
{
    public function getAttendaceIssue(Request $request){
        $this-> validate($request, [
            'date' => 'required|date'
        ]);
        
                
        $date=$request->input('date');
        
        
        $attendance_issues = AttendanceIssue::select(\Illuminate\Support\Facades\DB::raw('*,GROUP_CONCAT(status) as statuses'))
                                              ->whereHas('attendance_history',function($query) use($date){
                                                 $query->where('date','=',$date)->where('resolve','=','0');
                                              })
                                              ->groupBy('attendance_history_id')
                                              ->get();
                                              
        foreach ($attendance_issues as $att) {
            $att->statuses = array_map('intval', explode(',',$att->statuses));
        }
        
        
        return View::make('attendance/AttendanceIssues')->with (array(
                                                                        'attendance_issues' => $attendance_issues,
                                                                            'date'=> $date));
    }
    
    public function viewAddCheckoutTime(){
    }
    
    public function addCheckoutTime(Request $request){
    }
    
    public function viewAddCheckinTime(Request $request){
        $att_his_id = Route::input('his_id');
        $att_isu_id = Route::input('isu_id');
        
        $attendance_issue= AttendanceIssue::find($att_isu_id);
        
        return View::make('attendance/AddCheckinTime',array('attendance_issue'=>$attendance_issue));
        
    }
    
    public function addCheckinTime(Request $request){
        
        //no need to validate time as we are using a time picker- then invalid time cant be entered
        
        $att_his_id = Route::input('his_id');
        
        //getting the attendance history, employee id and date from the attendance history id
        $att_his= AttendanceHistory::find($att_his_id);
        $emp_id=$att_his->emp_id;
        $date=$att_his->date;
        
        if($att_his->check_in_time==null && $att_his->check_out_time!=null){
            $this->validate($request, [
                'checkInTime' => 'required'
            ]);
           //getting the attendance issue for the id
            $att_isu=AttendanceIssue::where('attendance_history_id','=',$att_his_id)
                                            ->where('status','=',2)->first();
            $att_isu_id=$att_isu->id;

            $checkOutTime=$att_his->check_out_time;         //get the check_out_time from the DB record
            $checkInTime=$request->input('checkInTime');    //get the input
            
            if($checkOutTime>$checkInTime){
            
            //adding the newly entered check_in_time to DB
            $att_his->check_in_time=$checkInTime;

            //removing att issue record for non-check-in
            $att_isu->resolve=1;  
            $att_isu->save();
            
            //splitting from 'time' to hours,minutes and seconds
            list($checkOutHour,$checkOutMinute,$checkOutSecond)=  explode(":", $checkOutTime);
            list($checkInHour,$checkInMinute,$checkInSecond)=  explode(":", $checkInTime);

            //setting the late_minutes and early_minutes variables
                if((intval($checkInHour)<12)){
                        if((intval($checkInHour)-8)>=0 ){
                            if((intval($checkInHour)-8)==0 ){
                                if((intval($checkInMinute)>0 )){
                                    $att_his->late_minutes=$checkInMinute;
                                    $att_his->early_minutes=0;
                                }elseif ((intval($checkInMinute)==0 )) {
                                    $att_his->late_minutes=0;
                                    $att_his->early_minutes=0;
                                }
                            }else{
                                $att_his->late_minutes=(((intval($checkInHour))-8)*60)+intval($checkInMinute);
                                $att_his->early_minutes=0;
                            }
                        }else{
                            if(intval($checkInMinute)>0){
                                $att_his->early_minutes=((8-(intval($checkInHour)))*60)+ (60-(intval($checkInMinute))-60);
                                $att_his->late_minutes=0;
                            }else{
                                $att_his->early_minutes=((8-(intval($checkInHour)))*60);
                                $att_his->late_minutes=0;
                            }
                        }
                }elseif (intval($checkInHour>=12)) {
                        //half day
                }
                
                //setting overtime_minutes and overtime_hours
                    if((intval($checkOutHour)-17)>=0 ){
                       if((intval($checkOutHour)-17)==0 ){
                            if((intval($checkOutMinute)>0 )){
                                $att_his->over_time_minutes=intval($checkInMinutes);
                                $att_his->over_time_hours=intval((intval($checkInMinute)/60));
                                //overtime
                            }elseif ((intval($checkInMinute)==0 )) {
                                $att_his->over_time_minutes=0;
                                $att_his->over_time_hours=0;
                            }

                        }else{
                            $att_his->over_time_minutes=((((intval($checkOutHour))-17))*60)+intval($checkOutMinute);
                            $att_his->over_time_hours=intval((intval($att_his->over_time_minutes)/60));
                            //overtime
                        }
                    }else{
                        //employee has checked out ealier
                    }
            if((intval($checkInTime)<12) &&  (intval($checkOutHour)==17) && (intval($checkOutMinute)==0)){
                //no overtime/ no check in or check out issues/ no leave 
            }

            //adding work time hours
            if(intval($checkOutHour)>17){
                $newCheckOutTime="17:00:00";
            }else{
                $newCheckOutTime=$checkOutTime;
            }
            if(intval($checkInHour)<8){
                $newCheckInTime="8:00:00";
            }else{
                $newCheckInTime=$att_his->check_in_time;
            }
            sscanf($newCheckInTime, "%d:%d:%d", $hours, $minutes, $seconds);

            $checkIn_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;

            sscanf($newCheckOutTime, "%d:%d:%d", $hours, $minutes, $seconds);

            $checkOut_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
            $att_his->work_time_hours=($checkOut_seconds-$checkIn_seconds)/3600;

            $att_his->save();

            //adding records to attendance issue table
                if(($checkInHour>=12) && intval($checkInMinute>=0) ){
                    $att_issue = new \App\AttendanceIssue();
                    $att_issue->attendance_history_id=$att_his->id;
                    $att_issue->status=4;       //Half day
                    $att_issue->save();
                }
                if(($checkOutTime==null)){
                    $att_issue = new \App\AttendanceIssue();
                    $att_issue->attendance_history_id=$att_his->id;
                    $att_issue->status=3;       //no check out
                    $att_issue->save();

                }
            }else{
                $attendance_issue= AttendanceIssue::find($att_isu_id);
                
                Session::flash('error', 'Check-In and Check-Out times are not compatible');
                return view(('attendance.addCheckinTime'),array('attendance_issue'=>$attendance_issue)); 
            }
        }
        
        elseif($att_his->check_in_time!=null && $att_his->check_out_time==null){
            $this->validate($request, [
                'checkOutTime' => 'required'
            ]);
            $att_isu=AttendanceIssue::where('attendance_history_id','=',$att_his_id)
                                            ->where('status','=',3)->first();
            $att_isu_id = $att_isu->id;
            
            $checkInTime=$att_his->check_in_time;       //get the preset check in time value
            $checkOutTime=$request->input('checkOutTime');  //get the input
            
            if ($checkOutTime>$checkInTime){
            //adding the newly entered check out time to DB
            $att_his->check_out_time=$checkOutTime;

            //removing att issue record for non-check-out
            $att_isu->resolve=1;  
            $att_isu->save();
            
            //setting overtime_minutes and overtime_hours variable
            list($checkOutHour,$checkOutMinute,$checkOutSecond)=  explode(":", $checkOutTime);

            list($checkInHour,$checkInMinute,$checkInSecond)=  explode(":", $checkInTime);

                    if((intval($checkOutHour)-17)>=0 ){
                       if((intval($checkOutHour)-17)==0 ){
                            if((intval($checkOutMinute)>0 )){
                                $att_his->over_time_minutes=intval($checkInMinutes);
                                $att_his->over_time_hours=intval((intval($checkInMinute)/60));
                                //overtime
                            }elseif ((intval($checkInMinute)==0 )) {
                                $att_his->over_time_minutes=0;
                                $att_his->over_time_hours=0;
                            }

                        }else{
                            $att_his->over_time_minutes=((((intval($checkOutHour))-17))*60)+intval($checkOutMinute);
                            $att_his->over_time_hours=intval((intval($att_his->over_time_minutes)/60));
                            //overtime
                        }
                    }else{
                        //employee has checked out ealier
                    }
                    
            if((intval($checkInTime)<12) &&  (intval($checkOutHour)==17) && (intval($checkOutMinute)==0)){
                //no overtime/ no check in or check out issues/ no leave 
            }

                if(intval($checkOutHour)>17){
                    $newCheckOutTime="17:00:00";
                }else{
                    $newCheckOutTime=$checkOutTime;
                }
                if(intval($checkInHour)<8){
                    $newCheckInTime="8:00:00";
                }else{
                    $newCheckInTime=$att_his->check_in_time;
                }
                sscanf($newCheckInTime, "%d:%d:%d", $hours, $minutes, $seconds);

                $checkIn_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;

                sscanf($newCheckOutTime, "%d:%d:%d", $hours, $minutes, $seconds);

                $checkOut_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
                $att_his->work_time_hours=($checkOut_seconds-$checkIn_seconds)/3600;

                $att_his->save();

                //adding records to attendance issue table
                    if(intval($checkOutHour>12) && intval($checkOutHour<17) ){
                        $att_issue = new \App\AttendanceIssue();
                        $att_issue->attendance_history_id=$att_his->id;
                        $att_issue->status=4;       //Half day 
                        $att_issue->save();
                    }if($checkOutTime!=null && intval($checkOutHour)>17 && intval($checkOutMinute)>=0){
                        $att_issue = new \App\AttendanceIssue();
                        $att_issue->attendance_history_id=$att_his->id;
                        $att_issue->status=5;       //Overtime
                        $att_issue->save();
                    }
                }else{
                        $attendance_issue= AttendanceIssue::find($att_isu_id);
                        
                        Session::flash('error', 'Check-In and Check-Out times are not compatible');
                        return view(('attendance.addCheckinTime'),array('attendance_issue'=>$attendance_issue)); 
                }
            }elseif($att_his->check_in_time==null && $att_his->check_out_time==null){
                $this->validate($request, [
               'checkInTime' => 'required',
               'checkOutTime' => 'required'
                ]);
                
                //getting the attendance issue for the id for No check in
                $att_isu_check_out=AttendanceIssue::where('attendance_history_id','=',$att_his_id)
                                                ->where('status','=',2)->first();
                
                
                //getting the attendance issue for the id for No check out
                $att_isu_check_in=AttendanceIssue::where('attendance_history_id','=',$att_his_id)
                                                ->where('status','=',3)->first();
                
                
                //getting the input
                $checkInTime=$request->input('checkInTime');
                $checkOutTime=$request->input('checkOutTime');
                
                if ($checkOutTime>$checkInTime){
                //adding the newly entered check_in_time and check-out-time to DB
                $att_his->check_in_time=$checkInTime;
                $att_his->check_out_time=$checkOutTime;

                //removing att issue records for non-check-in and non-check-out
                $att_isu_check_in->resolve=1;  $att_isu_check_in->save();
                $att_isu_check_out->resolve=1;  $att_isu_check_out->save();

                //splitting from 'time' to hours,minutes and seconds
                list($checkOutHour,$checkOutMinute,$checkOutSecond)=  explode(":", $checkOutTime);
                list($checkInHour,$checkInMinute,$checkInSecond)=  explode(":", $checkInTime);

                //setting the late_minutes and early_minutes variables
                    if((intval($checkInHour)<12)){
                            if((intval($checkInHour)-8)>=0 ){
                                if((intval($checkInHour)-8)==0 ){
                                    if((intval($checkInMinute)>0 )){
                                        $att_his->late_minutes=$checkInMinute;
                                        $att_his->early_minutes=0;
                                    }elseif ((intval($checkInMinute)==0 )) {
                                        $att_his->late_minutes=0;
                                        $att_his->early_minutes=0;
                                    }
                                }else{
                                    $att_his->late_minutes=(((intval($checkInHour))-8)*60)+intval($checkInMinute);
                                    $att_his->early_minutes=0;
                                }
                            }else{
                                if(intval($checkInMinute)>0){
                                    $att_his->early_minutes=((8-(intval($checkInHour)))*60)+ (60-(intval($checkInMinute))-60);
                                    $att_his->late_minutes=0;
                                }else{
                                    $att_his->early_minutes=((8-(intval($checkInHour)))*60);
                                    $att_his->late_minutes=0;
                                }
                            }
                    }elseif (intval($checkInHour>=12)) {
                            //half day
                    }

                    //setting overtime_minutes and overtime_hours
                        if((intval($checkOutHour)-17)>=0 ){
                           if((intval($checkOutHour)-17)==0 ){
                                if((intval($checkOutMinute)>0 )){
                                    $att_his->over_time_minutes=intval($checkInMinute);
                                    $att_his->over_time_hours=intval((intval($checkInMinute)/60));
                                    //overtime
                                }elseif ((intval($checkInMinute)==0 )) {
                                    $att_his->over_time_minutes=0;
                                    $att_his->over_time_hours=0;
                                }

                            }else{
                                $att_his->over_time_minutes=((((intval($checkOutHour))-17))*60)+intval($checkOutMinute);
                                $att_his->over_time_hours=intval((intval($att_his->over_time_minutes)/60));
                                //overtime
                            }
                        }else{
                            //employee has checked out ealier
                        }
                if((intval($checkInTime)<12) &&  (intval($checkOutHour)==17) && (intval($checkOutMinute)==0)){
                    //no overtime/ no check in or check out issues/ no leave 
                }

                //adding work time hours
                if(intval($checkOutHour)>17){
                    $newCheckOutTime="17:00:00";
                }else{
                    $newCheckOutTime=$checkOutTime;
                }
                if(intval($checkInHour)<8){
                    $newCheckInTime="8:00:00";
                }else{
                    $newCheckInTime=$att_his->check_in_time;
                }
                sscanf($newCheckInTime, "%d:%d:%d", $hours, $minutes, $seconds);

                $checkIn_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;

                sscanf($newCheckOutTime, "%d:%d:%d", $hours, $minutes, $seconds);

                $checkOut_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
                $att_his->work_time_hours=($checkOut_seconds-$checkIn_seconds)/3600;

                $att_his->save();

                //adding records to attendance issue table
                    if(($checkInHour>=12) && intval($checkInMinute>=0) ){
                        $att_issue = new \App\AttendanceIssue();
                        $att_issue->attendance_history_id=$att_his->id;
                        $att_issue->status=4;       //Half day
                        $att_issue->save();
                    }if($checkOutTime!=null && intval($checkOutHour)>17 && intval($checkOutMinute)>=0){
                        $att_issue = new \App\AttendanceIssue();
                        $att_issue->attendance_history_id=$att_his->id;
                        $att_issue->status=5;       //Overtime
                        $att_issue->save();
                    }
                
                }else{
                        $attendance_issue= AttendanceIssue::find($att_isu_check_in->id);
                        
                        Session::flash('error', 'Check-In and Check-Out times are not compatible');
                        return view(('attendance.addCheckinTime'),array('attendance_issue'=>$attendance_issue)); 
                }
                
            }
                $attendance_issues = AttendanceIssue::select(\Illuminate\Support\Facades\DB::raw('*,GROUP_CONCAT(status) as statuses'))
                                              ->whereHas('attendance_history',function($query) use($date){
                                                 $query->where('date','=',$date)->where('resolve','=','0');
                                              })
                                              ->groupBy('attendance_history_id')
                                              ->get();
                foreach ($attendance_issues as $att) {
                    $att->statuses = array_map('intval', explode(',',$att->statuses));
                }
                
                Session::flash('success', 'Issue Resolved');
                 return View::make('attendance/AttendanceIssues')->with (array(
                                                                        'attendance_issues' => $attendance_issues,
                                                                            'date'=> $date));
    }
}

