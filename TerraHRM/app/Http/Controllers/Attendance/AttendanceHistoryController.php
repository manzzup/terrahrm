<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use App\AttendanceHistory;
use Illuminate\Support\Facades\View;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AttendanceHistoryController extends Controller
{
    public function getAttendanceHistory(Request $request){
        $this->validate($request, [
            'date' => 'required|date'
        ]);
        
        $date=$request->input('date');
        
        $attendance_histories = AttendanceHistory::whereDate('date','=',$date)->get();
                
        return View::make('attendance/AttendanceHistory', array('attendance_histories' => $attendance_histories),array('date' => $date));
    }
    
}
