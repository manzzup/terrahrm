<?php

namespace App\Http\Controllers\Attendance;

use Illuminate\Http\Request;
use App\Employee;
use Session;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UploadAttendanceController extends Controller
{
    public function uploadFile(Request $request){
        $this->validate($request, [
            'date' => 'required|date',
            //the browser only lets us select txt files. so no validation required.
        ]);
        
        if ($request->hasFile('file')) {
           if ($request->file('file')->isValid()) {
               
            
            $date= $request->input('date');
            
            $file = fopen($request->file->getPathname(), "r");
            
            while (($line = fgets($file)) !== false) {                
                if(!feof($file)){
                    if(!preg_match('/^[a-zA-Z0-9_]{3,10},[01][0-9]:[0-5][0-9]:[0-5][0-9],[01][0-9]:[0-5][0-9]:[0-5][0-9]$|^[a-zA-Z0-9_]{3,10},[01][0-9]:[0-5][0-9]:[0-5][0-9],-$|^[a-zA-Z0-9_]{3,10},-,[01][0-9]:[0-5][0-9]:[0-5][0-9]$|^[a-zA-Z0-9_]{3,10},-,-$/im', substr($line, 0,-2))){
                        Session::flash('error', 'Invalid File!');
                        return Redirect::to('attendance/UploadAttendance');
                    }
                        list($id ,$checkInTime, $checkOutTime) = explode(",", $line);

                        $checkOutTime = substr($checkOutTime,0,-2);

                        if($checkOutTime!='-'){
                            $outTime = date_create_from_format('H:i:s',$checkOutTime);
                        }else{
                            $outTime=null;
                        }

                        if($checkInTime!='-'){
                            $inTime = date_create_from_format('H:i:s',$checkInTime);
                        }else{
                            $inTime=null;
                        }
                }else{
                        if(!preg_match('/^[a-zA-Z0-9_]{3,10},[01][0-9]:[0-5][0-9]:[0-5][0-9],[01][0-9]:[0-5][0-9]:[0-5][0-9]$|^[a-zA-Z0-9_]{3,10},[01][0-9]:[0-5][0-9]:[0-5][0-9],-$|^[a-zA-Z0-9_]{3,10},-,[01][0-9]:[0-5][0-9]:[0-5][0-9]$|^[a-zA-Z0-9_]{3,10},-,-$/im', substr($line, 0,-2))){
                            Session::flash('error', 'Invalid File!');
                            return Redirect::to('attendance/UploadAttendance');
                        }
                        list($id ,$checkInTime, $checkOutTime) = explode(",", $line);

                        if($checkOutTime!='-'){
                            $outTime = date_create_from_format('H:i:s',$checkOutTime);
                        }else{
                            $outTime=null;
                        }

                        if($checkInTime!='-'){
                            $inTime = date_create_from_format('H:i:s',$checkInTime);
                        }else{
                            $inTime=null;
                        }
                }
                $att_history = new \App\AttendanceHistory();
                
                $att_history->emp_id=Employee::where ('emp_id','=',$id)->first()->id;
                $att_history->check_in_time=$inTime;
                $att_history->check_out_time=$outTime;
                $att_history->date=$date;
                
                if($checkInTime!='-'){
                    list($checkInHour,$checkInMinute,$checkInSecond)=  explode(":", $checkInTime);
                }
                //setting the late_minutes and early_minutes variables
                if($checkInTime!='-' && (intval($checkInHour)<12)){
                    
                        if((intval($checkInHour)-8)>=0 ){

                            if((intval($checkInHour)-8)==0 ){
                                if((intval($checkInMinute)>0 )){
                                    $att_history->late_minutes=$checkInMinute;
                                    $att_history->early_minutes=0;
                                }elseif ((intval($checkInMinute)==0 )) {
                                    $att_history->late_minutes=0;
                                    $att_history->early_minutes=0;
                                }

                            }else{
                                $att_history->late_minutes=(((intval($checkInHour))-8)*60)+intval($checkInMinute);
                                $att_history->early_minutes=0;
                            }
                        }else{
                            if(intval($checkInMinute)>0){
                                $att_history->early_minutes=((8-(intval($checkInHour)))*60)+ (60-(intval($checkInMinute))-60);
                                $att_history->late_minutes=0;
                            }else{
                                $att_history->early_minutes=((8-(intval($checkInHour)))*60);
                                $att_history->late_minutes=0;
                            }
                        }
                }elseif ($checkInTime!='-' && intval($checkInHour>=12)) {
                        //half day
                    }
                else{
                    //no check in has been recorded
                }
                
                //setting overtime_minutes and overtime_hours variable
                if(($checkOutTime)!= '-'){
                        list($checkOutHour,$checkOutMinute,$checkOutSecond)=  explode(":", $checkOutTime);

                        if((intval($checkOutHour)-17)>=0 ){
                           if((intval($checkOutHour)-17)==0 ){
                                if((intval($checkOutMinute)>0 )){
                                    $att_history->over_time_minutes=intval($checkOutMinute);
                                    $att_history->over_time_hours=intval((intval($checkOutMinute)/60));
                                    //overtime
                                }elseif ((intval($checkOutMinute)==0 )) {
                                    $att_history->over_time_minutes=0;
                                    $att_history->over_time_hours=0;
                                }

                            }else{
                                $att_history->over_time_minutes=((((intval($checkOutHour))-17))*60)+intval($checkOutMinute);
                                $att_history->over_time_hours=intval((intval($att_history->over_time_minutes)/60));
                                //overtime
                            }
                        }else{
                            //employee has checked out ealier
                        }
                }else{
                    //no check out has been recorded
                }
                
                if(($checkInTime)!='-' && (intval($checkInTime)<12) && (($checkOutTime)!='-') && (intval($checkOutHour)==17) && (intval($checkOutMinute)==0)){
                    //no overtime/ no check in or check out issues/ no leave 
                }
                
                if((($checkInTime!='-'))&& ($checkOutTime!='-')){
                    
                    if(intval($checkOutHour)>17){
                        $newCheckOutTime="17:00:00";
                    }else{
                        $newCheckOutTime=$checkOutTime;
                    }
                    if(intval($checkInHour)<8){
                        $newCheckInTime='8:00:00';
                    }else{
                        $newCheckInTime=$checkInTime;
                    }
                    sscanf($newCheckInTime, "%d:%d:%d", $hours, $minutes, $seconds);

                    $checkIn_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
                    
                    sscanf($newCheckOutTime, "%d:%d:%d", $hours, $minutes, $seconds);

                    $checkOut_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
                    $att_history->work_time_hours=($checkOut_seconds-$checkIn_seconds)/3600;
                    
                    
                }
                
                $att_history->save();
                
                //adding records to attendance issue table
                        if(($checkInTime!='-' && $checkInHour>=12) && intval($checkInMinute>=0) ){
                            $att_issue = new \App\AttendanceIssue();
                            $att_issue->attendance_history_id=$att_history->id;
                            $att_issue->status=4;       //Half day
                            $att_issue->save();
                            
                        }if(($checkOutTime!='-') && intval($checkOutHour>=12) && ($checkOutMinute>=0) && intval($checkOutHour<17) ){
                            $att_issue = new \App\AttendanceIssue();
                            $att_issue->attendance_history_id=$att_history->id;
                            $att_issue->status=4;       //Half day
                            $att_issue->save();
                        }
                        if(($checkInTime=='-')){
                            $att_issue = new \App\AttendanceIssue();
                            $att_issue->attendance_history_id=$att_history->id;
                            $att_issue->status=2;       //no check in
                            $att_issue->save();
                        }if(($checkOutTime=='-')){
                            $att_issue = new \App\AttendanceIssue();
                            $att_issue->attendance_history_id=$att_history->id;
                            $att_issue->status=3;       //no checkout
                            $att_issue->save();
                        }if($checkOutTime!='-' && intval($checkOutHour)>17 && intval($checkOutMinute)>=0){
                            $att_issue = new \App\AttendanceIssue();
                            $att_issue->attendance_history_id=$att_history->id;
                            $att_issue->status=5;       //Overtime
                            $att_issue->save();
                        }
                        
                        
                
                
            } 
               
        }else{
            
            Session::flash('error', 'Attendance File field is required!');
            return Redirect::to('attendance/UploadAttendance');
        
        }
        }else{
            
            Session::flash('error', 'Attendance File field is required!');
            return Redirect::to('attendance/UploadAttendance');
        }
            Session::flash('uploadSuccess', 'Upload successful.'); 
            return Redirect::to('attendance/UploadAttendance');
        
}   
        
        
    
    
   
    

}
