<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LeaveController extends Controller
{
    public function viewLeaveTypes(){
        $leaveTypes = \App\LeaveType::all();
        return View::make('admin/leave/LeaveType',array('lvs' => $leaveTypes));
    }
    public function addLeaveType(Request $request){
        $this->validate($request, [
            'leaveType' => 'required'
        ]);
        $leaveName = $request->input('leaveType');
        $leave = new \App\LeaveType();
        $leave->leave_title = $leaveName;
        $leave->save();
        
        return redirect(route('admin.leave.viewLeaveTypes'));
    }
    public function removeLeaveType(Request $request,$leaveId){
        $leaveType = \App\LeaveType::whereId($leaveId)->first();
        if($leaveType){
            $leaveType->delete();
        }
        return redirect(route('admin.leave.viewLeaveTypes'));
    }
    
    public function viewLeavePeriod(){
        $leavePeriods = \App\LeavePeriod::all();
        return View::make('admin/leave/LeavePeriod',array('lvs' => $leavePeriods));
    }
    public function addLeavePeriod(Request $request){
        $this->validate($request, [
            'leaveYear' => 'required|numeric|min:2010|max:2100|unique:th_hr_leave_periods,leave_year',
            'startDate' => 'required|date|before:endDate',
            'endDate' => 'required|date|after:startDate'
        ]);
        
        $leavePeriod = new \App\LeavePeriod();
        $leavePeriod->leave_year = $request->input('leaveYear');
        $leavePeriod->from_date = $request->input('startDate');
        $leavePeriod->to_date = $request->input('endDate');
        $leavePeriod->save();
        
        
        return redirect(route('admin.leave.viewLeavePeriods'));
    }
    public function removeLeavePeriod(Request $request,$leavePeriod){
        $leavePeriod = \App\LeavePeriod::whereId($leavePeriod)->first();
        if($leavePeriod){
            $leavePeriod->delete();
        }
        return redirect(route("admin.leave.viewLeavePeriods"));
    }
}
