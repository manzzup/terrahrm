<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PIMController extends Controller
{
    public function addDepartment(Request $request){
        $this->validate($request, [
            'dptName' => 'required'
        ]);
        $dptName = $request->input('dptName');
        $dpt = new \App\Department();
        $dpt->name = $dptName;
        $dpt->save();
        
        return Redirect::route('admin.pim.viewDepartments');
        
    }
    
    public function viewDepartments(){
        $departments = \App\Department::all();
        return View::make('admin/pim/Department',array('dpts' => $departments));
    }
    
    public function removeDepartment(Request $request,$dptId){
        $department = \App\Department::whereId($dptId)->first();
        if($department){
            $department->delete();
        }
        return Redirect::route('admin.pim.viewDepartments');
    }
    
    public function viewGOC(){
        $gocs = \App\GroupOfCompany::all();
        return View::make('admin/pim/GOC',array('gocs' => $gocs));
    }
    
    public function removeGOC(Request $request,$gocID){
        $goc = \App\GroupOfCompany::whereId($gocID)->first();
        if($goc){
            $goc->delete();
        }
        return Redirect::route('admin.pim.viewGOC');
    }
    
    public function addGOC(Request $request){
        $this->validate($request, [
            'gocName' => 'required'
        ]);
        $gocName = $request->input('gocName');
        $goc = new \App\GroupOfCompany();
        $goc->name = $gocName;
        $goc->save();
        
        return Redirect::route('admin.pim.viewGOC');
        
    }
    
    public function viewJobCategories(){
        $jobs = \App\JobCategory::all();
        return View::make('admin/pim/JobCategory',array('jobs' => $jobs));
    }
    
    public function removeJobCategories(Request $request,$jobID){
        $job = \App\JobCategory::whereId($jobID)->first();
        if($job){
            $job->delete();
        }
        return Redirect::route('admin.pim.viewJobCategories');
    }
    
    public function addJobCategories(Request $request){
        $this->validate($request, [
            'jobName' => 'required'
        ]);
        $jobName = $request->input('jobName');
        $job = new \App\JobCategory();
        $job->category_name = $jobName;
        $job->save();
        
        return Redirect::route('admin.pim.viewJobCategories');
        
    }
    
    public function viewEmploymentStatuses(){
        $statuses = \App\EmploymentStatus::all();
        return View::make('admin/pim/EmploymentStatus',array('statuses' => $statuses));
    }
    
    public function removeEmploymentStatuses(Request $request,$statusID){
        $status = \App\EmploymentStatus::whereId($statusID)->first();
        if($status){
            $status->delete();
        }
        return Redirect::route('admin.pim.viewEmploymentStatuses');
    }
    
    public function addEmploymentStatuses(Request $request){
        $this->validate($request, [
            'statusName' => 'required'
        ]);
        $statusName = $request->input('statusName');
        $status = new \App\EmploymentStatus();
        $status->title = $statusName;
        $status->save();
        
        return Redirect::route('admin.pim.viewEmploymentStatuses');
        
    }
    
}
