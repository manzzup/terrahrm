<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function listUsers(){
       // print( Auth::user()->role_id);
        
        if(Auth::check()){
            $user = Auth::user();
            if($user->can('listAll',$user)){
                print "fsfsfdsf";
            }
        }
        
        $users = User::all();
        return view('admin.user.list')->with('users',$users);
    }
    
    public function viewUserRoles(){
        $roles = \App\UserRole::all();
        return View::make('admin/user/UserRoles',array('roles' => $roles));
       
    }
    
    public function addUserRole(Request $request){
        $this->validate($request, [
            'roleName' => 'required'
        ]);
        $roleName = $request->input('roleName');
        $role = new \App\UserRole();
        $role->role_name = $roleName;
        $role->save();
        
        return Redirect::route('admin.user.viewUserRoles');
    }
    
    public function removeUserRole(Request $request,$rid){
        $role = \App\UserRole::find($rid);
        
        if($role === null){
            return Redirect::route('admin.user.viewUserRoles')->withErrors(['msg' => 'Invalid role id']);
        }
        
            
        if(!$role->users->isEmpty()){
            return Redirect::route('admin.user.viewUserRoles')->withErrors(['msg' => "Role has assigned users, remove them first"]);
        }
        
        if($role->delete()){
            return Redirect::route('admin.user.viewUserRoles')->withSuccess("Role removed successfully");
        }else{
            return Redirect::route('admin.user.viewUserRoles')->withErrors(['msg' => "Error occurred"]);
        }
        
    }
    
    
    public function viewUsers(){
        $users = \App\User::where('enabled','=',1)->get();
        $roles = \App\UserRole::all();
        return View::make('admin/user/Users',array('users' => $users),array('roles' => $roles));
    }
    
    public function addUser(Request $request){
        $this->validate($request, [
            'username' => 'required|min:3|unique:users,username',
            'password' => 'required|confirmed|min:3',
            'empID' => 'required',
            'role' => 'required'
        ]);
        
        $employee = \App\Employee::find($request->get('empID'));
        if($employee == null){
            return Redirect::route('admin.user.viewUsers')->withErrors(['msg'=>'Invalid Employee Selected']);
        }
        
        $checkExist = \App\User::where('emp_id','=',$request->get('empID'))->get();
        if(!$checkExist->isEmpty()){
            return Redirect::route('admin.user.viewUsers')->withErrors(['msg'=>'Employee is already associated with a user account']);
        }
        
        $user = new \App\User();
        $user->emp_id = $request->get('empID');
        $user->username = $request->get('username');
        $user->password = \Illuminate\Support\Facades\Hash::make($request->get('password'));
        $user->role_id = $request->get('role');
        $user->enabled = 1;
        
        if($user->save()){
            return Redirect::route('admin.user.viewUsers');
        }else{
            return Redirect::route('admin.user.viewUsers')->withErrors(['msg'=>'Error creating user']);
        }
    }
    
    public function removeUser(Request $request,$uid){
        if($uid == Auth::user()->id){
            return Redirect::route('admin.user.viewUsers')->withErrors(['msg'=>'Cannot remove current user']);
        }
        $user = User::find($uid);
        if($user->username === 'admin'){
            return Redirect::route('admin.user.viewUsers')->withErrors(['msg'=>'Cannot remove Super User']);            
        }
        if($user !== null){
            $user->enabled = 0;
            $user->save();
        }
        
        return Redirect::route('admin.user.viewUsers');
    }
    
    public function viewAssignPermission(Request $request){
        $roles = \App\UserRole::all();
        if(isset($request->role)){
            $rid = $request->role;
            if(\App\UserRole::find($rid) !== null){
                $perms = \App\UserPermission::all();
                $sel_perms = \App\UserRolePermission::select(DB::raw('GROUP_CONCAT(perm_id) as array'))
                                                  ->where('role_id',$rid)
                                                  ->first();
                
                $sel_perms_array = array_map('intval',explode(',',$sel_perms->array));
                
                return View::make('admin/user/AssignPermissions')->with('roles',$roles)
                                                                 ->with('perms',$perms)
                                                                 ->with('sel_perms',$sel_perms_array);
            }
        }
        
        return View::make('admin/user/AssignPermissions',array('roles' => $roles));
    }
    
    public function assignPermission(Request $request){
        $rid = $request->role;
        if(\App\UserRole::find($rid) === null){
            Redirect::route('admin.user.assignPerm')->withErrors(['msg'=>'Invalid role selected']);
        }
        
        $perms = $request->perm_group;
        \App\UserRolePermission::where('role_id',$rid)->delete();
        foreach ($perms as $p) {
            $rolePerm = new \App\UserRolePermission();
            $rolePerm->role_id = $rid;
            $rolePerm->perm_id = $p;
            $rolePerm->save();
        }
        return Redirect::route('admin.user.assignPerm',['role' => $rid]);
    }
}
