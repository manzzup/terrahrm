<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $perm){
        if(!$request->user()->role->permissions->contains($perm)){
            return redirect()->route('dashboard');
        }

        return $next($request);
    }

}