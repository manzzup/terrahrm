<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveEntitlement extends Model
{
    protected $table = 'th_hr_leave_entitlements';
    
    public function leave_type(){
        
        return belongsTo('App\LeaveType','leave_type','id');
        
    }
    
    public function leave_period(){
        
        return belongsTo('App\LeavePeriod','leave_period','id');
        
    }
    
    public function employee(){
        
        return belongsTo('App\Employee','emp_id','id');
        
    }
    
}
    

