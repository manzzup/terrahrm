<?php

function getAuthUserName(){
    if(Auth::user() == null){
        return "";
    }
    $emp = \App\Employee::find(Auth::user()->emp_id);
    return $emp->first_name;
}

function getAuthUser(){
    return Auth::user();
}

function isActive($current){
    if(starts_with(Route::currentRouteName(),$current)){
        return 'active';
    }
    return '';
}
