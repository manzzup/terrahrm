<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveApplication extends Model
{
    protected $table = 'th_hr_leave_applications';
    
    public function employee(){
        
        return $this->belongsTo('App\Employee','emp_id','id');
        
    }
    
    public function leave_type_obj(){
        
        return $this->hasOne('App\LeaveType','id','leave_type');
        
    }
    
    public function issue_obj(){
        
        return $this->hasOne('App\AttendanceIssue','id','issue_date');
        
    }
    
    
}
