<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollInformation extends Model
{
    protected $table='th_hr_payroll_informations';
    
    public function employee(){
        return belongsTo('App\Employee','id','emp_id');
    }
    
    public function pay_grade(){
        return belongsTo('App\PayGrade','id','pay_grade');
    }
}
