<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeContactDetail extends Model
{
    protected $table = 'th_hr_employee_contact_details';
    
    public function employee(){
        return belongsTo('App\Employee','id','emp_id');
    }
}
