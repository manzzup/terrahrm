<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceIssue extends Model
{
    protected $table='th_hr_attendance_issues';
    
    public function attendance_history(){
        return $this->belongsTo('App\AttendanceHistory','attendance_history_id','id');
    }
}
