<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{
    protected $table = 'th_sys_user_permissions';
    public $primaryKey = 'perm_id';
}
