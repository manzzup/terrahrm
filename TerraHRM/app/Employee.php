<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'th_hr_employees';
  
    public function contact_details(){
        return hasOne('App\EmployeeContactDetails','emp_id','id');
    }
    public function employment_details(){
        return $this->hasOne('App\EmployeeEmploymentDetail','emp_id','id');
        
    }
    
    public function payroll_information(){
        return $this->hasOne('App\PayrollInformation','emp_id','id');
    }
    
    public function payroll_transactions(){
        return $this->hasMany('App\PayrollTransaction','emp_id','id');
    }
    
    public function attendance_histories(){
        return $this->hasMany('App\AttendanceHistory','emp_id','id');
    }
    
    
//    public function dependent_details(){
//        return hasMany('App\EmployeeDependentDetails','emp_id','id');
//    }
//    public function emergency_contact_details(){
//       return hasMany('App\EmployeeEmergencyContactDetails','emp_id','id');
//    }
}
