<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeavePeriod extends Model
{
    protected $table = 'th_hr_leave_periods';
}
