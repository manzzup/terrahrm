<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupOfCompany extends Model
{
    protected $table = 'th_sys_group_of_companies';
}
