<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function access_admin_module(User $user){
        return $user->role->permissions->contains(1);
    }
    
    public function access_admin_pim(User $user){
        return $user->role->permissions->contains(2);
    }
    
    public function access_admin_leave(User $user){
        return $user->role->permissions->contains(3);
    }
    
    public function access_admin_user(User $user){
        return $user->role->permissions->contains(6);
    }
    
       
    
    
    public function access_pim_module(User $user){
        return $user->role->permissions->contains(14);
    }
    public function access_pim_view(User $user){
        return $user->role->permissions->contains(15);
    }
    public function access_pim_add(User $user){
        return $user->role->permissions->contains(16);
    }
    public function access_pim_edit(User $user){
        return $user->role->permissions->contains(17);
    }
    
    
    public function access_leave_module(User $user){
        return $user->role->permissions->contains(24);
    }
    public function access_leave_assign(User $user){
        return $user->role->permissions->contains(25);
    }
    public function access_leave_history(User $user){
        return $user->role->permissions->contains(26);
    }
    public function access_leave_entitle(User $user){
        return $user->role->permissions->contains(27);
    }
    public function access_leave_balance(User $user){
        return $user->role->permissions->contains(28);
    }
    
    
    public function access_attendance_module(User $user){
        return $user->role->permissions->contains(18);
    }
    public function access_attendance_upload(User $user){
        return $user->role->permissions->contains(19);
    }
    public function access_attendance_history(User $user){
        return $user->role->permissions->contains(20);
    }
    public function access_attendance_issues(User $user){
        return $user->role->permissions->contains(21);
    }
    public function access_attendance_checkin(User $user){
        return $user->role->permissions->contains(22);
    }
    public function access_attendance_checkout(User $user){
        return $user->role->permissions->contains(23);
    }
}
