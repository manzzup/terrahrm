<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeEmploymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('th_hr_employee_employment_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id')->unsigned();
            $table->foreign('emp_id')->references('id')->on('th_hr_employees');
            $table->integer('department')->unsigned();
            $table->foreign('department')->references('id')->on('th_sys_departments');
            $table->integer('employment_status')->unsigned();
            $table->foreign('employment_status')->references('id')->on('th_sys_employment_statuses');
            $table->integer('job_category')->unsigned();
            $table->foreign('job_category')->references('id')->on('th_sys_job_categories');
            $table->string('job_title');
            $table->date('joined_date');
            $table->string('leaving_reason');

            $table->integer('group_of_companies')->unsigned();
            $table->foreign('group_of_companies')->references('id')->on('th_sys_group_of_companies');

            $table->boolean('hr_active_flag');

            $table->date('left_date');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('th_hr_employee_employment_details');
    }
}
