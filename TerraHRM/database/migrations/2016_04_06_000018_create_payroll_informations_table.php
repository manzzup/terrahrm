<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('th_hr_payroll_informations', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('emp_id')->unsigned();
            $table->foreign('emp_id')->references('id')->on('th_hr_employees');
            
            $table->integer('pay_grade')->unsigned();
            $table->foreign('pay_grade')->references('id')->on('th_hr_pay_grades');
            
            $table->string('epf_number');
            $table->string('etf_number');
            
            $table->timestamps();
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('th_hr_payroll_informations');
    }
}
