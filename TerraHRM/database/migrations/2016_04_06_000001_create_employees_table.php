<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('th_hr_employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('emp_id');
            $table->string('full_name');
            $table->string('first_name');
            $table->string('last_name');
            $table->boolean('gender');
            $table->integer('marital_status');
            $table->date('date_of_birth');
            $table->string('nic');
            $table->string('nationality');
            $table->string('license');
            $table->string('avatar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('th_hr_employees');
    }
}
