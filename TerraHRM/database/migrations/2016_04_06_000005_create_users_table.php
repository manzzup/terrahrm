<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username',32);
            $table->string('password',64);
            $table->string('remember_token',100)->nullable();
            $table->integer('emp_id')->unsigned();
            $table->foreign('emp_id')->references('id')->on('th_hr_employees');
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('role_id')->on('user_roles');
            $table->boolean('enabled')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
