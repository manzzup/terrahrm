<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveEntitlementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            
            Schema::create('th_hr_leave_entitlements', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('department')->unsigned();
            $table->foreign('department')->references('id')->on('th_sys_departments');
            $table->integer('leave_type')->unsigned();
            $table->foreign('leave_type')->references('id')->on('th_hr_leave_types');
            $table->integer('leave_period')->unsigned();
            $table->foreign('leave_period')->references('id')->on('th_hr_leave_periods');
            $table->integer('emp_id')->unsigned();
            $table->foreign('emp_id')->references('id')->on('th_hr_employees');
            $table->integer('entitlements');
            $table->timestamps();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('th_hr_leave_entitlements');
    }
}
