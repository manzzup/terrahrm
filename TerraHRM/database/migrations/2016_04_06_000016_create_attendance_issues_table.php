<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('th_hr_attendance_issues', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('attendance_history_id')->unsigned();
            $table->foreign('attendance_history_id')->references('id')->on('th_hr_attendance_histories');

            $table->integer('status');
            $table->boolean('resolve');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('th_hr_attendance_issues');
    }
}
