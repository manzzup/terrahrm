<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('th_hr_payroll_transactions', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('emp_id')->unsigned();
            $table->integer('transaction_type_id')->unsigned();
            $table->float('amount');  
            $table->integer('units');
            $table->float('percentage');
            $table->float('percentage_type_id');
            
            $table->foreign('emp_id')->references('id')->on('th_hr_employees');
            $table->foreign('transaction_type_id')->references('id')->on('th_hr_transaction_types');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('th_hr_payroll_transactions');
    }
}
