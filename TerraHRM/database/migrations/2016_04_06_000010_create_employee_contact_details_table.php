<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeContactDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('th_hr_employee_contact_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id')->unsigned();
            $table->foreign('emp_id')->references('id')->on('th_hr_employees');
            $table->string('add_line_1');
            $table->string('add_line_2');
            $table->string('city');
            $table->string('province');
            $table->string('home_no');
            $table->string('work_no');
            $table->string('mobile_no');
            $table->string('work_email');
            $table->string('personal_email');
            $table->string('emergency_name');
            $table->string('emergency_relationship');
            $table->string('emergerncy_contact_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('th_hr_employee_contact_details');
    }
}
