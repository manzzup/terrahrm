<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('th_hr_attendance_histories', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('emp_id')->unsigned();
            $table->foreign('emp_id')->references('id')->on('th_hr_employees');
            $table->time('check_in_time')->nullable();;
            $table->time('check_out_time')->nullable();;
            $table->integer('late_minutes');
            $table->integer('early_minutes');
            $table->integer('over_time_minutes');
            $table->integer('over_time_hours');
            $table->double('work_time_hours');
            $table->float('attendance_rate');

            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('th_hr_attendance_histories');
    }
}
