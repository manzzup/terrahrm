<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('th_hr_leave_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id')->unsigned();
            $table->foreign('emp_id')->references('id')->on('th_hr_employees');
            $table->integer('leave_type')->unsigned();
            $table->foreign('leave_type')->references('id')->on('th_hr_leave_types');

            $table->integer('issue_id')->unsigned();
            $table->foreign('issue_id')->references('id')->on('th_hr_attendance_issues');
            
            $table->string('comment');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('th_hr_leave_applications');
    }
}
