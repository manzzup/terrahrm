<?php

use Illuminate\Database\Seeder;

use App\AttendanceHistory;

class AttendanceHistorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AttendanceHistory::create(array(
            'emp_id'=>'1',
            'check_in_time'=>'08:30:00',
            'check_out_time'=>'17:15:00',
            'late_minutes'=>'15',
            'early_minutes'=>'55',
            'over_time_minutes'=>'3',
            'over_time_hours'=>'0',
            'work_time_hours'=>'5',
            'attendance_rate'=>'13.8',
            'status' => '0',
            'date'=>'2016-05-19'
        ));
        
        AttendanceHistory::create(array(
            'emp_id'=>'2',
            'check_in_time'=>'09:00:00 am',
            'check_out_time'=>'20:20:00 pm',
            'late_minutes'=>'45',
            'early_minutes'=>'0',
            'over_time_minutes'=>'185',
            'over_time_hours'=>'3',
            'work_time_hours'=>'8',
            'attendance_rate'=>'23.0',
            'status' => '1',
            'date'=>'2016-05-20'
        ));
        
        AttendanceHistory::create(array(
            'emp_id'=>'1',
            'check_in_time'=>'08:00:00 am',
            'check_out_time'=>'18:15:00 pm',
            'late_minutes'=>'0',
            'early_minutes'=>'15',
            'over_time_minutes'=>'60',
            'over_time_hours'=>'1',
            'work_time_hours'=>'8',
            'attendance_rate'=>'45.0',
            'status' => '2',
            'date'=>'2016-05-20'
        ));
        
        AttendanceHistory::create(array(
            'emp_id'=>'2',
            'check_in_time'=>'08:00:00 am',
            'check_out_time'=>'18:15:00 pm',
            'late_minutes'=>'0',
            'early_minutes'=>'15',
            'over_time_minutes'=>'60',
            'over_time_hours'=>'1',
            'work_time_hours'=>'8',
            'attendance_rate'=>'45.0',
            'status' => '3',
            'date'=>'2016-05-20'
        ));
    }
}
