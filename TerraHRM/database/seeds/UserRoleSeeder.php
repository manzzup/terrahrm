<?php

use App\UserRole;
use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->delete();
        UserRole::create(array(
            'role_name' => 'Administrator'
        ));
        UserRole::create(array(
            'role_name' => 'Manager'
        ));
        UserRole::create(array(
            'role_name' => 'Staff Officer'
        ));
        UserRole::create(array(
            'role_name' => 'Employee'
        ));
    }
}
