<?php

use Illuminate\Database\Seeder;
use App\AttendanceIssue;

class AttendanceIssueSeeder extends Seeder
{
    
    public function run()
    {
        AttendanceIssue::create(array(
            'attendance_history_id' => '4',
            'status'=>'1'
        ));
        
        AttendanceIssue::create(array(
            'attendance_history_id' => '5',
            'status'=>'2'
        ));
        
        AttendanceIssue::create(array(
            'attendance_history_id' => '6',
            'status'=>'3'
        ));
        
        
    }
}
