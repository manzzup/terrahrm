<?php

use Illuminate\Database\Seeder;
use App\LeaveApplication;

class LeaveApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('th_hr_leave_applications')->delete();
        LeaveApplication::create(array(
            
            'emp_id' => '1',
            'leave_type' => '1',
            'from_date' => '20-05-2016',
            'to_date' => '25-05-2016',
            'leave_days' => '5',
            'comment'=> 'Child Birth',
            'status' => '0'
            
        ));
        LeaveApplication::create(array(
            
            'emp_id' => '1',
            'leave_type' => '2',
            'from_date' => '2-02-2016',
            'to_date' => '2-02-2016',
            'leave_days' => '1',
            'comment'=> 'Surgery',
            'status' => '1'
            
        ));
        
        LeaveApplication::create(array(
            
            'emp_id' => '1',
            'leave_type' => '1',
            'from_date' => '20-12-2016',
            'to_date' => '28-12-2016',
            'leave_days' => '8',
            
            'status' => '1'
            
        ));
        
        LeaveApplication::create(array(
            
            'emp_id' => '1',
            'leave_type' => '2',
            'from_date' => '20-05-2015',
            'to_date' => '25-05-2015',
            'leave_days' => '5',
             
            'status' => '0'
            
        ));
    }
}
