<?php

use App\UserPermission;
use Illuminate\Database\Seeder;

class UserPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('th_sys_user_permissions')->delete();
        $statement = "ALTER TABLE th_sys_user_permissions AUTO_INCREMENT = 1;";
        DB::unprepared($statement);
        
        UserPermission::create(array(
            'perm_name' => 'Access Admin module',
            'parent' => 0
        ));
        UserPermission::create(array(
            'perm_name' => 'Access Admin PIM module',
            'parent' => 1
        ));
        UserPermission::create(array(
            'perm_name' => 'Access Admin Leave module',
            'parent' => 1
        ));
        UserPermission::create(array(
            'perm_name' => 'Access Admin Attendance module',
            'parent' => 1
        ));
        UserPermission::create(array(
            'perm_name' => 'Access Admin Payroll module',
            'parent' => 1
        ));

        //Admin - Users
        UserPermission::create(array(
            'perm_name' => 'Access Admin User',
            'parent' => 1
        ));
        UserPermission::create(array(
            'perm_name' => 'Add User'
        ));
        UserPermission::create(array(
            'perm_name' => 'Edit/Remove User'
        ));
        
        //Admin - User Roles
        UserPermission::create(array(
            'perm_name' => 'Access Admin User Role'
        ));
        UserPermission::create(array(
            'perm_name' => 'Add User Role'
        ));
        UserPermission::create(array(
            'perm_name' => 'Edit/Remove User Role'
        ));
        
        //Admin - User Permissions
        UserPermission::create(array(
            'perm_name' => 'Access Admin User Permissions'
        ));
        UserPermission::create(array(
            'perm_name' => 'Change User Permissions'
        ));
        
        //PIM Module
        UserPermission::create(array(
            'perm_name' => 'Access PIM module',
            'parent' => 0
        ));
        UserPermission::create(array(
            'perm_name' => 'View Employees',
            'parent' => 14
        ));
        UserPermission::create(array(
            'perm_name' => 'Add New Employee',
            'parent' => 14
        ));
        UserPermission::create(array(
            'perm_name' => 'Edit Employee Details',
            'parent' => 14
        ));
        
        //attendance
        UserPermission::create(array(
            'perm_name' => 'Access Attendance module',
            'parent' => 0
        ));
        UserPermission::create(array(
            'perm_name' => 'Uplaod Attendance File',
            'parent' => 18
        ));
        UserPermission::create(array(
            'perm_name' => 'View Attendance History',
            'parent' => 18
        ));
        UserPermission::create(array(
            'perm_name' => 'View Attendance Issues',
            'parent' => 18
        ));
        UserPermission::create(array(
            'perm_name' => 'Resolve Check-in issues',
            'parent' => 18
        ));
        UserPermission::create(array(
            'perm_name' => 'Resolve Check-out issues',
            'parent' => 18
        ));
        
        //leave
        UserPermission::create(array(
            'perm_name' => 'Access Leave module',
            'parent' => 0
        ));
        UserPermission::create(array(
            'perm_name' => 'Assign Leave',
            'parent' => 24
        ));
        UserPermission::create(array(
            'perm_name' => 'View Leave History',
            'parent' => 24
        ));
        UserPermission::create(array(
            'perm_name' => 'Add Leave Entitlement',
            'parent' => 24
        ));
        UserPermission::create(array(
            'perm_name' => 'View Leave Balance',
            'parent' => 24
        ));
    }
}
