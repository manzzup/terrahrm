<?php

use Illuminate\Database\Seeder;
use App\leave_type;

class LeaveTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('th_hr_leave_types')->delete();
        leave_type::create(array(
            'leave_type_id' => 01,
            'leave_type' => casual,
        ));
    }
}
