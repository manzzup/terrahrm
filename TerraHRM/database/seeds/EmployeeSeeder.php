<?php
use App\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('th_hr_employees')->delete();
        Employee::create(array(
            'emp_id' => 'EMP001',
            'full_name' => 'Manujith Pallewatte',
            'last_name' => 'Pallewatte'
        ));
    }
}
