<?php

use App\UserRolePermission;
use Illuminate\Database\Seeder;

class UserRolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('th_sys_user_role_permissions')->delete();
        UserRolePermission::create(array(
            'role_id' => 1,
            'perm_id' => 1
        ));
        UserRolePermission::create(array(
            'role_id' => 1,
            'perm_id' => 6
        ));    
        
    }
}
