<?php

use Illuminate\Database\Seeder;
use App\UserPermission;

class UserPermissionSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        //Admin - Users
        UserPermission::create(array(
            'perm_name' => 'Access Admin User'
        ));
        UserPermission::create(array(
            'perm_name' => 'Add User'
        ));
        UserPermission::create(array(
            'perm_name' => 'Edit/Remove User'
        ));
        
        //Admin - User Roles
        UserPermission::create(array(
            'perm_name' => 'Access Admin User Role'
        ));
        UserPermission::create(array(
            'perm_name' => 'Add User Role'
        ));
        UserPermission::create(array(
            'perm_name' => 'Edit/Remove User Role'
        ));
        
        //Admin - User Permissions
        UserPermission::create(array(
            'perm_name' => 'Access Admin User Permissions'
        ));
        UserPermission::create(array(
            'perm_name' => 'Change User Permissions'
        ));
        
    }
}
