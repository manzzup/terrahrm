function selectIssue(e){
    window.opener.callback($(e).data('id'),$(e).data('status'),$(e).data('date'),$(e).data('eid'),$(e).data('ename'));
    window.close();
}

function openSearchAttendanceIssueDialog(url,callback){
    window.callback = callback;
    window.open (url, "searchWindow","location=no,toolbar=no, menubar=no,status=yes,scrollbars=1");
}

function applyLeaveCallback(isu_id,status,date,eid,ename){
    $('#isuID').val(isu_id);
    $('#status').val("No Check-in");
    $('#date').val(date);
    $('#empID').val(eid);
    $('#empID_2').val(ename);    
}