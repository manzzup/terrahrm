function selectEmp(e){
    //alert($(e).data('id'));
    window.opener.callback($(e).data('id'),$(e).data('empid'),$(e).data('name'));
    window.close();
}

function openSearchEmployeeDialog(url,callback){
    window.callback = callback;
    window.open (url, "searchWindow","location=no,toolbar=no, menubar=no,status=yes,scrollbars=1");
}

function leaveEntitlementCallback(id,emp_id,emp_name){
    $('#empID').val(id);
    $('#empID_2').val(emp_id);
    $('#empName').val(emp_name);
}

function leaveHistoryCallback(id,emp_id,emp_name){
    $('#empID').val(id);
    $('#empID_2').val(emp_id);
    $('#empName').val(emp_name);
}

function addUserCallback(id,emp_id,emp_name){
    $("#empID").val(id);
    $("#empName").val(emp_id + " : " + emp_name);
}

function leaveBalanceCallback(id,emp_id,emp_name){
    $("#empID").val(id);
    $("#empName").val(emp_id + " : " + emp_name);
}