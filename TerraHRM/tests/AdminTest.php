<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
class AdminTest extends TestCase
{
    public function makeUser(){
        $user = User::first();
        $this->be($user);
    }
    
    public function testViewUser(){
        $this->makeUser();
        $this->visit('/admin/user/Users')
             ->see('admin');
    }
    
    public function testAddUser(){
        $this->makeUser();
        $this->visit('/admin/user/Users')
             ->type('adminTest','username')
             ->type('admin','password')
             ->type('admin','password_confirmation')
             ->type('8','empID')
             ->select(1,'role')
             ->press('Add')
             ->see('adminTest');
    }
}
