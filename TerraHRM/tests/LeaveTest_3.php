<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class LeaveTest_3 extends TestCase
{
    public function makeUser(){
        $user = User::first();
        $this->be($user);
    }
    
    // Test Apply Leave here
    
    public function testLeaveEntitlement(){
        $this->makeUser();
        $this->visit('/leave/ViewEntitlement')
            
                
             ->select(2,'department')
             ->type('2','empID')
                
             ->select(2,'leaveTypes')
             ->select(1,'leavePeriod')
             ->type('6','entitlement')
             ->press('Save')
             ->see('Successfully');
    }
}
