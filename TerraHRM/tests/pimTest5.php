<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class pimTest5 extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function makeUser(){
        $user = User::first();
        $this->be($user);
    }
     public function testSearchEmployee(){
        $this->makeUser();
        $this->visit('/pim/ViewEmployee')
            ->select(1,'searchBy')
             ->type('EMP008','query')
             ->press('Search')
            ->see('siripe');
    }
     public function testSearchEmployee2(){
        $this->makeUser();
        $this->visit('/pim/ViewEmployee')
            ->select(2,'searchBy')
             ->type('Tamasha','query')
             ->press('Search')
            ->see('Malepathirana');
    }
      public function testSearchEmployee3(){
        $this->makeUser();
        $this->visit('/pim/ViewEmployee')
            ->select(3,'searchBy')
                ->select(3,'department')
             
             ->press('Search')
            ->see('Perera');
    }
}
