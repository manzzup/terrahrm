<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class pimtest3 extends TestCase
{
    public function makeUser(){
        $user = User::first();
        $this->be($user);
    }
      public function testUpdateContactDetail(){
        $this->makeUser();
        $this->visit('/pim/2/ViewContactDetails')
             ->type('linetest','line1')
             ->type('linetest','line2')  
             ->type('citytest','city')
            ->type('provincetest','province')
             ->type('0775480379','workNo')
             ->type('tamasha.14@cse.mrt.ac.lk','workMail')
             ->type('name','name')
                ->type('relationship','relationship')
                ->type('23747','contactNo')
                
             ->press('Save')
            ->see('Succesfully');
      }
}
