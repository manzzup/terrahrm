<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class LeaveTest_2 extends TestCase
{
    public function makeUser(){
        $user = User::first();
        $this->be($user);
    }
    
    // Test Apply Leave here
    
    public function testApplyLeave(){
        $this->makeUser();
        $this->visit('/leave/ApplyLeave')
            
                
             ->type('2','isuID')
             ->type('2','empID')
                
             ->select(5,'leaveTypes')
             ->press('Assign')
             ->see('Successfully');
    }
}
