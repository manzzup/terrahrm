<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
class pimtest2 extends TestCase
{
    public function makeUser(){
        $user = User::first();
        $this->be($user);
    }
      public function testUpdatePersonalDetail(){
        $this->makeUser();
        $this->visit('/pim/1/ViewPersonalDetails')
             ->type('full','fullName')
             ->type('first','firstName')  
             ->type('last','lastName')
            ->select(0,'gender')
             ->select(1,'maritalStatus')
             ->type(' ','nicNo')
             ->press('Save')
            ->see('Succesfully');
    }
   
}
