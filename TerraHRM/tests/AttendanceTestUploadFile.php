<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class AttendanceTestUploadFile extends TestCase
{
    
    public function makeUser(){
        $user = User::first();
        $this->be($user);
    }
    
    public function testAttendanceHistory(){
        $this->makeUser();
//        $this->visit('/attendance/UploadAttendance')
//             ->attach('C:\Users\admin\Desktop\Terra\Test.txt','file')
//             ->press('Submit')
//             ->see('Upload Successful');
        $this->visit('/attendance/AttendanceHistory')
             ->type('2016-05-17','date')
             ->press('Search')
             ->see('Savindrie');   
    }
    
    public function testAttendanceIssue(){
        $this->makeUser();
        $this->visit('/attendance/AttendanceIssues')
             ->type('2016-05-17','date')
             ->press('Search')
             ->see('Savindrie');   
    }
}
