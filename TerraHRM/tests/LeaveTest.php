<?php

// Test Leave Summary here


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class LeaveTest extends TestCase
{
    public function makeUser(){
        $user = User::first();
        $this->be($user);
    }
    
    // Test Leave Summary here
    
    public function testLeaveSummary(){
        $this->makeUser();
        $this->visit('/leave/LeaveSummary')
             ->type('2016-05-17','fromDate')
             ->type('2016-05-18','toDate')
                
             ->type('Kalana','empName')
             ->type(2,'empID')
                
            // ->select(2,'department')
             ->press('Search')
             ->see('casual');
    }
}
