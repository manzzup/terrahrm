<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class pimtest4 extends TestCase
{
    public function makeUser(){
        $user = User::first();
        $this->be($user);
    }
      public function testUpdateEmploymentDetail(){
        $this->makeUser();
        $this->visit('/pim/1/ViewEmploymentDetails')
                 ->type('testTitle','jobTitle')
               
                ->type('2016-05-17','joinedDate')
                 ->type('2016-05-17','leftDate')
               
                
            ->press('Save')
            ->see('succesfully');
      }
}
