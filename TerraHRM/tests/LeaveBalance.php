<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')

@section('content')
    
 
<form class="form-horizontal" role="form">
    <div class="form-group">
        <label class="control-label col-sm-4" for="empName">Employee:</label>
        
        <div class="col-sm-4">
            <input type="text" class="form-control" id="name" name="name">
        </div>
    </div>
    
    
    
 </form>
 
<br>
     
  <table class="table table-bordered table-striped" >
    <thead>
      <tr>
        <th>Leave Type</th>
        <th>Entitlement</th>
        <th>Balance</th>
        <th>Pending</th>
        <th>Taken</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Casual</td>
        <td>10</td>
        <td>6</td>
        <td>2</td>
        <td>2</td>
      </tr>
      <tr>
        <td>Medical</td>
        <td>5</td>
        <td>3</td>
        <td>0</td>
        <td>2</td>
      </tr>
      
    </tbody>
  </table>
</div>

    
     
    
@endsection