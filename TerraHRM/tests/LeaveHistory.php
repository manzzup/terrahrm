<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')

@section('content')
    
<div class="col-xs-12">
    <form class="form-horizontal" role="form">
        <div> <h4>  Search </h4> </div>
    
        <div class="form-group">
                <label class="control-label col-sm-2" for="fromDate">From :</label>
                <div class="col-sm-3">
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='date' class="form-control" />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>  
            
        
        
                <label class="control-label col-sm-2" for="fromDate">To :</label>
                <div class="col-sm-3">
                    <div class='input-group date' id='datetimepicker2'>
                        <input type='date' class="form-control" />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>  
            </div>
    
        
    <div class="form-group">
                <label class="control-label col-sm-2" for="fromDate">Leave Status :</label>
                <div class="checkbox col-sm-2">
                    <label><input type="checkbox" value=""> All  </label><br>                  
                    <label><input type="checkbox" value="">Rejected</label>
                    <label><input type="checkbox" value="">Approved</label>
                    <label><input type="checkbox" value="">Pending Approval</label>
                    <label><input type="checkbox" value="">Cancelled</label>
                </div>     
     
    
        </div>
    <div class="form-group ">
        <div> <h4>  Filter by </h4> </div>
        <label class="control-label col-sm-2" for="empName">Employee :</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="empName" >
      </div> 
    </div>
    
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="leaveTypes">Department :</label>
         <div class="col-sm-2"> 
                    <select class="form-control" id="leaveTypes" name="leaveTypes">
                        <option value="1">Select</option>
                        <option value="2">HR</option>
                        <option  value="3">Finance</option>
                        <option value="4">IT</option>
                        
                    </select>
                </div>
    </div>
    
    
        <br>
       <div> <h4>  Search Results </h4> </div> 
       <table class="table table-bordered table-striped" >
    <thead>
      <tr>
        <th>Leave Type</th>
        <th>Entitlement</th>
        <th>Balance</th>
        <th>Pending</th>
        <th>Taken</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Casual</td>
        <td>10</td>
        <td>6</td>
        <td>2</td>
        <td>2</td>
      </tr>
      <tr>
        <td>Medical</td>
        <td>5</td>
        <td>3</td>
        <td>0</td>
        <td>2</td>
      </tr>
      
    </tbody>
  </table>
        

     </form>
</div>  
@endsection