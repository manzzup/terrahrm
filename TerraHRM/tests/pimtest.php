<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
class pimtest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function makeUser(){
        $user = User::first();
        $this->be($user);
    }
    public function testAddEmployee(){
        $this->makeUser();
        $this->visit('/pim/AddEmployee')
             ->type('pimTest','firstName')
             ->type('last','lastName')
            
             ->type('EMP001','empId')
             
             ->press('Save')
            ->seePageIs('/pim/ViewPersonalDetail');
    }
   
}
