<!-- resources/views/tasks.blade.php -->
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
@extends( empty(app('request')->input('select')) ? 'layouts.app' : 'default')
@section('sidebar')

    <ul class="nav nav-sidebar">
        <li><a href="{{ route('attendance.upload') }}">Upload Daily Attendance</a></li>
        <li><a href="{{ route('attendance.history') }}">Attendance Summary</a></li>
        <li class='active'><a href="{{ route('attendance.issues') }}">Attendance Issues</a></li>
        <li><a href="{{ route('report.list','attendance') }}">Reports</a></li>
    </ul>

@endsection

@section('content')

@if(Session::has('success'))
          <div class="alert alert-success">
              <p>{!! Session::get('success') !!}</p>
          </div>
 @endif    
 
<form class="form-horizontal" role="form" action="{{ route('attendance.getIssues') }}@if(!empty(app('request')->input('select')))?select=true @endif @if(!empty(app('request')->input('action')))&action={{app('request')->input('action')}} @endif " method="post">
    
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 

    <h2>Attendance Issues</h2>
    <hr size='2'><br>
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="datetimepicker">Date:</label>
        
        <div class="col-sm-3">
            <div class='input-group date' id='date'>
                
                <input type='text' class="form-control" id="date" name="date" value="{{ isset($date) ? $date : '' }}">
                
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar">
                    </span>
                </span>
                
            </div>
            
        </div>
        <div class="col-sm-offset-1 col-sm-2">
                <button type="submit" class="btn btn-primary">Search</button>
        </div>
        
    </div>
   
   
 </form>
 

<br>  <br>
@if(isset($attendance_issues))
@if(!$attendance_issues -> isEmpty())
  <table class="table table-bordered table-striped" >
    <thead>
      <tr>
        <th>Date</th>
        <th>Employee</th>
        <th>Check-In time</th>
        <th>Check-Out time</th>
        <th>Status</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
           
                @foreach($attendance_issues as $att_issue)
                    
                    <tr>
                        <td> {{$att_issue->attendance_history->date}}</td>
                        
                        <td>{{ $att_issue->attendance_history->employee->first_name }} {{ $att_issue->attendance_history->employee->last_name }} </td>
                        <td>{{ $att_issue->attendance_history->check_in_time }}</td>
                        <td>{{ $att_issue->attendance_history->check_out_time}}</td>
                            <td>
                            @if(in_array(1,$att_issue->statuses))OK @endif
                            @if(in_array(2,$att_issue->statuses))No Check-In<br>@endif
                            @if(in_array(3,$att_issue->statuses))No Check-Out<br>@endif
                            @if(in_array(4,$att_issue->statuses))Half Day<br>@endif
                            @if(in_array(5,$att_issue->statuses))Overtime<br>@endif
                            
                            </td>
                            
                            <td>
                            @if(in_array(1,$att_issue->statuses))OK<br>@endif
                            @if(in_array(2,$att_issue->statuses) && (!in_array(3,$att_issue->statuses)))
                                                                @if(empty(app('request')->input('action')))
                                                                    <a href="{{ route('attendance.viewAddCheckin',[$att_issue->attendance_history_id,$att_issue->id]) }}"> Add Check In </a><br>     
                                                                @endif   
                                                                    
                            @endif
                            
                            @if(in_array(3,$att_issue->statuses) && (!in_array(2,$att_issue->statuses)))<a href="{{ route('attendance.viewAddCheckin',[$att_issue->attendance_history_id,$att_issue->id]) }}"> Add Check Out </a><br> @endif     
                            @if(in_array(2,$att_issue->statuses) && in_array(3,$att_issue->statuses))@if(empty(app('request')->input('action')))
                                                                    <a href="{{ route('attendance.viewAddCheckin',[$att_issue->attendance_history_id,$att_issue->id]) }}"> Add Check In/ Check Out </a><br>     
                                                                @endif   
                                                                    <a href="{{ route('leave.apply') }}" data-eid="{{ $att_issue->attendance_history->employee->id }}" data-ename="{{ $att_issue->attendance_history->employee->emp_id }}"  data-id="{{ $att_issue->id }}" data-status="{{ $att_issue->attendance_history->status }}" data-date="{{ $att_issue->attendance_history->date }}" @if(!empty(app('request')->input('select'))) onclick="selectIssue(this); return false;" @endif> Assign Leave </a><br>
                            @endif
                            @if(in_array(4,$att_issue->statuses))<a href="{{ route('leave.apply') }}" data-eid="{{ $att_issue->attendance_history->employee->id }}" data-ename="{{ $att_issue->attendance_history->employee->emp_id }}"  data-id="{{ $att_issue->id }}" data-status="{{ $att_issue->attendance_history->status }}" data-date="{{ $att_issue->attendance_history->date }}" @if(!empty(app('request')->input('select'))) onclick="selectIssue(this); return false;" @endif> Assign Leave </a><br>@endif
                            @if(in_array(5,$att_issue->statuses))No action<br>@endif
                            
                            
                    </tr>
                    
                     
                @endforeach
           
               
           

      
      
    </tbody>
  </table>
@else
<div class="alert alert-warning">
     <p>No search result found</p>
</div>
 @endif
@endif
<script type="text/javascript">
    $(function () {
        $('#date').datetimepicker({
            format: 'YYYY-MM-DD',
            defaultDate: 'now'
        });
    });
</script>
<script type="text/javascript" src="{{ asset('js/searchAttendanceIssue.js') }}"></script>


@endsection