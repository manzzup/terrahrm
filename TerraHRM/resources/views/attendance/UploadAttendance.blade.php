                                                          <!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
        <li class='active'><a href="{{ route('attendance.upload') }}">Upload Daily Attendance</a></li>
        <li><a href="{{ route('attendance.history') }}">Attendance Summary</a></li>
        <li><a href="{{ route('attendance.issues') }}">Attendance Issues</a></li>
        <li><a href="{{ route('report.list','attendance') }}">Reports</a></li>
    </ul>

@endsection

@section('content')
@if(Session::has('uploadSuccess') || Session::has('error'))
@if(Session::has('uploadSuccess'))
          <div class="alert alert-success">
              <p>{!! Session::get('uploadSuccess') !!}</p>
          </div>
 @else
            <div class="alert alert-warning">
               <p class="errors">{!! Session::get('error') !!}</p>
            </div>
 @endif 
 @endif
<div class="col-xs-12">
    <form class="form-horizontal" role="form" action="{{ route('attendance.uploadFile') }}" method="post" enctype="multipart/form-data">
        <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
        
        <h2>Upload Attendance</h2>
        <hr size='2'><br>
    
        <div class="form-group">
            
            <label class="control-label col-sm-2" for="date">Date:</label>
            <div class="col-sm-3">
            <div class='input-group date' id='date'>
                <input type='text' class="form-control" id="date" name="date" >
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar">
                    </span>
                </span>
                
            </div>
            
        </div>
        </div>
        
        
        <div class="form-group">
            <label class="control-label col-sm-2" for="file">Attendance File:</label> 
            <div class="form-group">
                <div class="col-sm-5">
                    <input type="file" class="field" id="file" name="file" accept=".txt">
                </div>
            </div>
        </div>
        
        
        
        <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary" >Submit</button>
            </div>
        </div>
        
        
    </form>
    
</div>
 
<script type="text/javascript">
    $(function () {
        $('#date').datetimepicker({
            format: 'YYYY-MM-DD',
            defaultDate: 'now'
        });
    });
</script>



@endsection    

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        