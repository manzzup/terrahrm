<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
        <li><a href="{{ route('attendance.upload') }}">Upload Daily Attendance</a></li>
        <li><a href="{{ route('attendance.history') }}">Attendance Summary</a></li>
        <li><a href="{{ route('attendance.issues') }}">Attendance Issues</a></li>
        <li class='active'><a href="{{ route('report.list','attendance') }}">Reports</a></li>
    </ul>

@endsection

@section('content')    
    
  <div class="row">
      
    <div class="col-xs-12">
       
        @include('common.listReports')
    
    </div>
  </div>


    
@endsection