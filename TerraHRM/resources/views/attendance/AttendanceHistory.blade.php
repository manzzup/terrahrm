<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
        <li><a href="{{ route('attendance.upload') }}">Upload Daily Attendance</a></li>
        <li class='active'><a href="{{ route('attendance.history') }}">Attendance Summary</a></li>
        <li><a href="{{ route('attendance.issues') }}">Attendance Issues</a></li>
        <li><a href="{{ route('report.list','attendance') }}">Reports</a></li>
    </ul>

@endsection

@section('content')

 
<form class="form-horizontal" role="form" action="{{ route('attendance.getHistory') }}" method="post">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
 
   <h2>Attendance Summary</h2>
    <hr size='2'><br>
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="date">Date:</label>
        
        <div class="col-sm-3">
            <div class='input-group date' id='date'>
                <input type='text' class="form-control" id="date" name="date" value="{{ isset($date) ? $date : '' }}">
                
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar">
                    </span>
                </span>
                
            </div>
            
        </div>
        
        <div class="col-sm-offset-1 col-sm-2">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        
    </div>

    
 </form>
 
<br>  <br>
@if(isset($attendance_histories))
 @if(!$attendance_histories-> isEmpty())
  <table class="table table-bordered table-striped" >
    <thead>
      <tr>
        <th>Date</th>
        <th>Employee Name</th>
        <th>Check-In time</th>
        <th>Check-Out time</th>
      </tr>
    </thead>
    <tbody>
           
                @foreach($attendance_histories as $att_history)
                    <tr>
                        <td> {{$att_history->date}}</td>
                        
                        <td>{{ $att_history->employee->first_name }} {{ $att_history->employee->last_name }}</td>
                        <td>{{ $att_history->check_in_time }}</td>
                        <td>{{$att_history->check_out_time}}</td>
                        </tr>
                     
                @endforeach
            
            
               
            

      
      
    </tbody>
  </table>
 @else
 
 <div class="alert alert-warning">
     <p>No search result found</p>
</div>
 
 @endif
 @endif
<script type="text/javascript">
    $(function () {
        $('#date').datetimepicker({
            format: 'YYYY-MM-DD',
            defaultDate: 'now'
        });
    });
</script>
    
@endsection