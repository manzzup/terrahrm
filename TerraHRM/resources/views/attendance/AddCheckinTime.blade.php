                                                          <!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
        <li><a href="{{ route('attendance.upload') }}">Upload Daily Attendance</a></li>
        <li><a href="{{ route('attendance.history') }}">Attendance Summary</a></li>
        <li class='active'><a href="{{ route('attendance.issues') }}">Attendance Issues</a></li>
        <li><a href="{{ route('report.list','attendance') }}">Reports</a></li>
    </ul>

@endsection

@section('content')
@if(Session::has('error'))
                <div class="alert alert-warning">
               <p class="errors">{!! Session::get('error') !!}</p>
            </div>
@endif 
<form class="form-horizontal" role="form" action="{{ route('attendance.viewAddCheckin',[$attendance_issue->attendance_history_id,$attendance_issue->id]) }}" method="post">
    
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>  
    
    <h2>Add Check In/ Check Out</h2>
    <hr size='2'><br>
    
    <div class="form-group">
        
        <label class="control-label col-sm-2" for="fullName">Full Name:</label>
        
        <div class="col-sm-3">
            <input type="text" class="form-control" id="fullName" name="fullName" value="{{$attendance_issue->attendance_history->employee->first_name }} {{$attendance_issue->attendance_history->employee->last_name }}"  readonly="" >
        </div>
    </div>
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="date">Date:</label>
        
        <div class="col-sm-3">
            <input type="text" class="form-control" id="date" name="date" value="{{$attendance_issue->attendance_history->date}}"  readonly="" >
        </div>
    </div>
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="checkInTime">Check In Time:</label>
        <div class="col-sm-3">
            
        @if($attendance_issue->attendance_history->check_in_time != null)
            <input type="text" class="form-control" id="checkInTime" name="checkInTime" value="{{$attendance_issue->attendance_history->check_in_time}}"  readonly="" >
            
        @elseif ($attendance_issue->attendance_history->check_in_time == null )
                <div class='input-group time' id='checkInTime'>
                    <input type="text" class="form-control" id='checkInTime' name="checkInTime">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar">
                            </span>
                        </span>
                </div>
         @endif
        
        </div>
    </div>
    
     <div class="form-group">
        <label class="control-label col-sm-2" for="checkOutTime">Check Out time:</label>
        
        <div class="col-sm-3">
            @if($attendance_issue->attendance_history->check_out_time == null)
            <div class='input-group time' id='checkOutTime'>
                <input type="text" class="form-control" id = 'checkOutTime' name="checkOutTime">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar">
                        </span>
                    </span>
            </div>
            
            @elseif ($attendance_issue->attendance_history->check_out_time != null)
            <input type="text" class="form-control" id="checkOutTime" name="checkOutTime" value="{{$attendance_issue->attendance_history->check_out_time}}"  readonly="" >
            
            
            @endif
            
        </div>
    </div>
    
    
    
    
    <hr size="2">
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-3">
                <button type="submit" class="btn btn-primary">Save and Continue</button>
        </div>
    </div>
        
        <script type="text/javascript">
            $(function () {
                $('#checkInTime').datetimepicker({
                    format: 'HH:mm:ss'
                });
            });
            $(function () {
                $('#checkOutTime').datetimepicker({
                    format: 'HH:mm:ss'
                });
            });
        </script>
  
            
          
</form>
@endsection                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  