                                                          <!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
        <li class='active'><a href="{{ route('attendance.upload') }}">Upload Daily Attendance</a></li>
        <li><a href="{{ route('attendance.history') }}">Attendance Summary</a></li>
        <li><a href="{{ route('attendance.issues') }}">Attendance Issues</a></li>
    </ul>

@endsection

@section('content')
<form class="form-horizontal" role="form" action="{{ route('attendance.viewAddCheckout',[$attendance_issue->attendance_history_id,$attendance_issue->id]) }}" method="post">
    
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>  
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="fullName">Full Name:</label>
        
        <div class="col-sm-3">
            <input type="text" class="form-control" id="fullName" name="fullName" value="{{$attendance_issue->attendance_history->employee->first_name }} {{$attendance_issue->attendance_history->employee->last_name }}"  readonly="" >
        </div>
    </div>
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="date">Date:</label>
        
        <div class="col-sm-3">
            <input type="text" class="form-control" id="date" name="date" value="{{$attendance_issue->attendance_history->date}}"  readonly="" >
        </div>
    </div>
    
     <div class="form-group">
        <label class="control-label col-sm-2" for="checkInTime">Check In time:</label>
        
        <div class="col-sm-3">
            <input type="text" class="form-control" id="checkInTime" name="checkInTime" value="{{$attendance_issue->attendance_history->check_in_time}}"  readonly="" >
        </div>
    </div>
    
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="time">Check Out Time:</label>
        <div class="col-sm-3">
        <div class='input-group date' id='time'>
            <input type='text' class="form-control" name="time">
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar">
                </span>
            </span>

        </div>
        </div>
    </div>
    <hr size="2">
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-3">
                <button type="submit" class="btn btn-primary">Save and Continue</button>
        </div>
        </div>
        
        <script type="text/javascript">
            $(function () {
                $('#time').datetimepicker({
                    format: 'HH:mm:ss',
                defaultDate: 'now'
                });
            });
        </script>
  </div>
            
          
</form>
@endsection                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            