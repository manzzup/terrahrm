<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
        <li><a href="{{ route('leave.summary') }}">Leave Summary</a></li>
        <li class="active"><a href="{{ route('leave.apply') }}">Apply Leave</a></li>
        <li><a href="{{ route('leave.entitlement') }}">Leave Entitlement</a></li>                          
        <li><a href="{{ route('leave.balance') }}">Leave Balance</a></li>
        <li><a href="{{ route('report.list','leave') }}">Reports</a></li>
    </ul>

@endsection

@section('content')

<h2>Assign Leave</h2>
<hr>

@if(Session::has('flash_message'))
    <div class="alert alert-success">{!! session('flash_message') !!}</div>
@endif


<div class="col-xs-12">
    <form class="form-horizontal" role="form" action="{{ route('leave.apply') }}" method="post">
        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
<!--    <div class="form-group ">
        <label class="control-label col-sm-2" for="empName">Employee :</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="empName" name="empName" value="{{ old('empName') }}" readonly="" >
      </div>
        <div class="col-sm-1"> 
            <div class="col-sm-offset-2 col-sm-10">
                <button type="button" class="btn btn-default" onclick="openSearchEmployeeDialog('{{ route('pim.viewEmployee') }}?select=true',leaveHistoryCallback)">Select </button>
            </div>
        </div>
    </div>-->
    
    <div class="form-group ">
        <label class="control-label col-sm-2" for="empID">Employee ID :</label>
        <div class="col-sm-2">
            <input class="form-control col-sm-2" name="empID_2" for="empID" id="empID_2" readonly="" value="{{ old('empID_2') }}"> 
           <input type="hidden" class="form-control" id="empID" name="empID" readonly="" value="{{ old('empID') }}" >
        </div> 
       
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="leaveTypes">Attendance Issue :</label>
        
        <div class="col-sm-4">
            <input type="text" class="form-control" id="status" name="status" readonly="" value="{{ old('status') }}">
            <input type="hidden" class="form-control" id="isuID" name="isuID" readonly="" value="{{ old('isuID') }}">

        </div>
        <div class="col-sm-1"> 
            <div class="col-sm-offset-2 col-sm-10">
                <button type="button" class="btn btn-default" onclick="openSearchAttendanceIssueDialog('{{ route('attendance.issues') }}?select=true&action=leave',applyLeaveCallback)">Select </button>
            </div>
        </div>
    </div>    
        
    <div class="form-group">
        <label class="control-label col-sm-2" for="fromDate">Issue Date:</label>
        <div class="col-sm-3">
            <div class='input-group date' id='datetimepicker2'>
                <input type='text' readonly class="form-control" name="date" id="date" value="{{ old('date') }}" />
                <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>  
    </div>
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="leaveTypes">Leave Type :</label>
         <div class="col-sm-2"> 
                    <select class="form-control" id="leaveTypes" name="leaveTypes">
                        <option value="">Select</option>
                        @if(isset($leaveTypes))
                            @foreach($leaveTypes as $l)
                               <option value="{{ $l->id }}">{{$l->leave_title}}</option>

                           @endforeach
                       @else 

                       @endif
                    </select>
                </div>
    </div>
    
      
        
        <div class="form-group">
                <label  class="control-label col-sm-2" for="comment">Comment:</label>
                <div class="col-sm-4">
                    <textarea class="form-control" rows="5" id="comment" name="comment"></textarea>  
                </div>
            </div>
        <br>
          
       
            <div class="col-sm-offset-0 col-sm-10">
                <button type="submit" class="btn btn-primary">Assign</button>
            </div>
    </form>
</div> 


<script type="text/javascript" src="{{ asset('js/searchEmployee.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/searchAttendanceIssue.js') }}"></script>

@endsection



