<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
        <li><a href="{{ route('leave.summary') }}">Leave Summary</a></li>
        <li><a href="{{ route('leave.apply') }}">Apply Leave</a></li>
        <li class="active"><a href="{{ route('leave.entitlement') }}">Leave Entitlement</a></li>                          
        <li><a href="{{ route('leave.balance') }}">Leave Balance</a></li>
        <li><a href="{{ route('report.list','leave') }}">Reports</a></li>
    </ul>

@endsection

@section('content')

<h2>Add Entitlement to Employee</h2>
<hr>

@if(Session::has('flash_message'))
    <div class="alert alert-success">{!! session('flash_message') !!}</div>
@endif 
<div class="col-xs-12">
    <form class="form-horizontal" role="form" action="{{ route('leave.addEntitlement') }}" method="post">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
    <div class="form-group ">
        <label class="control-label col-sm-2" for="empName">Employee :</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="empName" name="empName" readonly="" value="{{ old('empName') }}">
            <input name="empID" id="empID" type="hidden" value="" value="{{ old('empID') }}" /> 
      </div> 
        <div class="col-sm-1"> 
            <div class="col-sm-offset-2 col-sm-10">
                <button type="button" class="btn btn-default" onclick="openSearchEmployeeDialog('{{ route('pim.viewEmployee') }}?select=true',leaveEntitlementCallback)">Search </button>
            </div>
        </div>
    </div>
    
    <div class="form-group ">
        <label class="control-label col-sm-2" for="empID">Employee ID :</label>
        <div class="col-sm-2">
            <input class="form-control col-sm-2" for="empID" id="empID_2" readonly="">
      </div> 
       
    </div>
<!--    
    <div class="form-group">
        <label class="control-label col-sm-2" for="leaveTypes">Department :</label>
         <div class="col-sm-2"> 
                    <select class="form-control" id="department" name="department">
                        <option value="">Select</option>
                        @if(isset($dpts))
                            @foreach($dpts as $d)
                               <option value="{{ $d->id }}">{{$d->name}}</option>

                           @endforeach
                       @else 

                       @endif
                        
                    </select>
                </div>
    </div>-->
    <input type="hidden" name="department" value="{{$dpts[0]->id}}" />
        
     <div class="form-group">
        <label class="control-label col-sm-2" for="leaveTypes">Leave Type :</label>
         <div class="col-sm-2"> 
                    <select class="form-control" id="leaveTypes" name="leaveTypes">
                        <option value="">Select</option>
                        @if(isset($leaveTypes))
                            @foreach($leaveTypes as $l)
                               <option value="{{ $l->id }}">{{$l->leave_title}}</option>

                           @endforeach
                       @else 

                       @endif
                    </select>
                </div>
    </div>
    
        
    <div class="form-group">
        <label class="control-label col-sm-2" for="leavePeriod">Leave Period :</label>
         <div class="col-sm-2"> 
                    <select class="form-control" id="leavePeriod" name="leavePeriod">
                        <option value="">Select</option>
                        @if(isset($leavePeriod))
                            @foreach($leavePeriod as $l)
                               <option value="{{ $l->id }}">{{$l->from_date}}-{{$l->to_date}}</option>

                           @endforeach
                       @else 

                       @endif

                    </select>
                </div>
    </div> 
    <div class="form-group ">
        <label class="control-label col-sm-2" for="entitlement">Entitlement :</label>
        <div class="col-sm-2">
            <input type="number" class="form-control" id="entitlement" name="entitlement" value="{{ old('entitlement') }}" >
      </div> 
    </div>
        
    
    <div class="form-group"> 
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
        

    <hr size=2>

     </form>
</div> 

<script type="text/javascript" src="{{ asset('js/searchEmployee.js') }}"></script>

@endsection
