<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
        <li class="active"><a href="{{ route('leave.summary') }}">Leave Summary</a></li>
        <li><a href="{{ route('leave.apply') }}">Apply Leave</a></li>
        <li><a href="{{ route('leave.entitlement') }}">Leave Entitlement</a></li>                          
        <li><a href="{{ route('leave.balance') }}">Leave Balance</a></li>
        <li><a href="{{ route('report.list','leave') }}">Reports</a></li>
    </ul>

@endsection

@section('content')

<h2>Leave Summary</h2>
<hr>

@if(isset($leave_histories))
@if($leave_histories->isEmpty())
    <div class="alert alert-info">{!! session('flash_message') !!}</div>
@endif
@endif
<h4>  Search </h4>     
<div class="col-xs-12">
    <form class="form-horizontal" role="form" method="post" action="{{ route('leave.searchEmployee') }}">
        
        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
        <div class="form-group">
                <label class="control-label col-sm-2" for="fromDate">From :</label>
                <div class="col-sm-3">
                    <div class='input-group date' id='fromDate'>
                        <input type='text' class="form-control" name="fromDate" value="{{ old('fromDate') }}" />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>  
            
        
        
                <label class="control-label col-sm-2" for="fromDate">To :</label>
                <div class="col-sm-3">
                    <div class='input-group date' id='toDate'>
                        <input type='text' class="form-control" name="toDate" value="{{old('toDate')}}"/>
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>  
            </div>
    
      
        
    <hr  align=center size=2>
     
    <div class="form-group ">
        
        <div class=""> <h4>Filter by </h4> </div>
        <label class="control-label col-sm-2" for="empName" >Employee :</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="empName" name="empName" value="{{old('empName')}}" readonly="">
      </div>
        <div class="col-sm-1"> 
            <div class="col-sm-offset-0 col-sm-10">
                <button type="button" class="btn btn-default" onclick="openSearchEmployeeDialog('{{ route('pim.viewEmployee') }}?select=true',leaveHistoryCallback)">Select </button>
            </div>
        </div>
    </div>
        
    <div class="form-group ">
        <label class="control-label col-sm-2" for="empID">Employee ID :</label>
        <div class="col-sm-2">
<!--            <label class="control-label col-sm-2" for="empID" id="empID_2"> (ID)</label>-->
             <input type="text" class="form-control " for="empID" id="empID_2" name="empID_2" value="{{old('empID_2')}}" readonly> 
           <input type="hidden" class="form-control" id="empID" name="empID" value="{{old('empID')}}" readonly="">
      </div> 
       
    </div>
    
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="leaveTypes">Department :</label>
         <div class="col-sm-2"> 
                    <select class="form-control" id="department" name="department">
                        <option value="">Select</option>
                        @if(isset($dpts))
                            @foreach($dpts as $d)
                               <option value="{{ $d->id }}">{{$d->name}}</option>

                           @endforeach
                       @else 

                       @endif
                        
                    </select>
                </div>
    </div>

    <hr  align=center size=2>
    </div>    
                <button type="submit" class="btn btn-primary">Search</button>
      
 
    <div class="col-xs-12">   
    
        <br>
        <br>
        <br>
              @if(isset($leave_histories))

       <div> <h4>  Search Results </h4> </div> 
       
       <table class="table table-bordered table-striped" >
    <thead>
      <tr>
        <th>Applied Date</th>
        <th>Employee</th>
        <th>Issue Date</th>
        <th>Leave Type</th>
        
<!--        <th>Status</th>-->
        <th>Comments</th>
      </tr>
    </thead>
    <tbody>
                @foreach($leave_histories as $lv_history)
                   
                      <tr>
                        <td> {{$lv_history->created_at}}</td>
                        <td> {{$lv_history->employee->first_name}}</td>  
                        <td> {{$lv_history->issue_obj->attendance_history->date}}</td> 
                        <td> {{$lv_history->leave_type_obj->leave_title}}</td> 
                        
<!--                        <td> {{$lv_history->status}}</td> -->
                        <td> {{$lv_history->comment}}</td> 
                      </tr>
                   
                @endforeach
          
      
    </tbody>
  </table>
       @else 
               
            @endif  
     </form>
 
    
</div>  

<script type="text/javascript" src="{{ asset('js/searchEmployee.js') }}"></script>
<script type="text/javascript">
            $(function () {
                $('#fromDate').datetimepicker({
                    format: 'YYYY-MM-DD',
                    defaultDate: 'now'
                });
                $('#toDate').datetimepicker({
                    format: 'YYYY-MM-DD',
                    defaultDate: 'now'
                });
            });
</script>
        
@endsection
