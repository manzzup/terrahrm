<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
        <li><a href="{{ route('leave.summary') }}">Leave Summary</a></li>
        <li><a href="{{ route('leave.apply') }}">Apply Leave</a></li>
        <li><a href="{{ route('leave.entitlement') }}">Leave Entitlement</a></li>                          
        <li class="active"><a href="{{ route('leave.balance') }}">Leave Balance</a></li>
        <li><a href="{{ route('report.list','leave') }}">Reports</a></li>
    </ul>

@endsection

@section('content')
    
<h2>Leave Balance</h2>
<hr>

 
<form class="form-horizontal" role="form" action="{{ route('leave.balance') }}" method="post">
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    @can('access_leave_assign',getAuthUser())
    <div class="form-group">
        <label class="control-label col-sm-1" for="empName">Employee:</label>
        
        <div class="col-sm-4">
            <input type="text" class="form-control" readonly id="empName" name="empName">
            <input type="hidden" name="empID" id='empID' />
        </div>
        <div class="col-sm-4">
                <button type="button" class="btn btn-default" onclick="openSearchEmployeeDialog('{{ route('pim.viewEmployee') }}?select=true',leaveBalanceCallback)">Select</button>
            </div>
    </div>
    <hr>
     <button type="submit" class="btn btn-primary" name="button1">Search</button>
     @else
     <button type="submit" class="btn btn-primary" name="button2">View My Balance</button>
     @endcan
 </form>
 
<br>
  @if(isset($results))     
  <table class="table table-bordered table-striped" >
    <thead>
      <tr>
        <th>Leave Type</th>
        <th>Entitlement</th>
        <th>Taken</th>
        <th>Balance</th>
        
      </tr>
    </thead>
    <tbody>
    @foreach($results as $r)
                
      <tr>
        <td> {{$r->leave_type}}</td>
        <td> {{$r->ent}}</td>
        <td> {{$r->taken}}</td>
        <td> {{$r->leave_balance}}</td>

      </tr>
         @endforeach
            
    </tbody>
  </table>
  @else
  @endif
</div>

    
<script type="text/javascript" src="{{ asset('js/searchEmployee.js') }}"></script>
    
@endsection
