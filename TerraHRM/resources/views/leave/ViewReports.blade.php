<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
        <li><a href="{{ route('leave.summary') }}">Leave Summary</a></li>
        <li><a href="{{ route('leave.apply') }}">Apply Leave</a></li>
        <li><a href="{{ route('leave.entitlement') }}">Leave Entitlement</a></li>                          
        <li><a href="{{ route('leave.balance') }}">Leave Balance</a></li>
        <li class="active"><a href="{{ route('report.list','leave') }}">Reports</a></li>
    </ul>

@endsection

@section('content')    
    
  <div class="row">
      
    <div class="col-xs-12">
       
        @include('common.listReports')
    
    </div>
  </div>


    
@endsection