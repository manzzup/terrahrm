@extends('layouts.app')

@section('sidebar')

    <ul class="nav nav-sidebar">
        <li class="active"><a href="#">Admin Dashboard</a></li>
    </ul>

@endsection

@section('content')
<script type="text/javascript" src="{{ URL('js/Chart.js') }}"></script>

<h2>Dashboard</h2>
<hr>

<div class="row">
    <div class="col-xs-5 content-box">
        <canvas id="pie1">

        </canvas>
        <div class="title">
            <h5 align="center">Department-wise Employee Distribution</h5>
        </div>
    </div>
    <div class="col-xs-2"></div>
    <div class="col-xs-5 content-box">
        <canvas id="pie2">

        </canvas>
        <div class="title"
            <h5 align="center">Group-wise Employee Distribution</h5>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-xs-12 content-box" style="text-align: center">
        <canvas id="line1">

        </canvas>
    </div>
</div>

<script type="text/javascript">
        var colors = ["#1f77b4","#ff7f0e","#2ca02c","#d62728","#9467bd","#8c564b","#e377c2","#7f7f7f","#bcbd22","#17becf"];
        function drawPieChart(ctx,labels,data,legend){
                var datas = {
                    datasets: [{
                        data: data,
                        backgroundColor: colors,
                        label: legend
                    }],
                    labels: labels
                };
                new Chart(ctx, {
                    data: datas,
                    type: 'pie'
                });
        }
        
        function drawLineChart(ctx,labels,data,legend){
                var datas = {
                    datasets: [{
                        data: data,
                        backgroundColor:colors[0],
                        borderColor: colors[0],
                        label: legend,
                        fill:false,
                        lineTension:0,
                        tension:0
                    }],
                    labels: labels
                };
                new Chart(ctx, {
                    data: datas,
                    type: 'line',
                    options: {
                        scales: {
                            yAxes: [{
                                display: true,
                                ticks: {
                                    suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                                    suggestedMax: 20,
                                    beginAtZero: true   // minimum value will be 0.
                                }
                            }]
                        }
                    }
                });
        }
        
        $.get('/dashboard/data',function(dataset){
            var pie1_data = dataset.pie1;
            drawPieChart($('#pie1'),pie1_data.labels,pie1_data.data,
                                   "Departments");
                                   
            var pie2_data = dataset.pie2;
            drawPieChart($('#pie2'),pie2_data.labels,pie2_data.data,
                                   "Groups");
                                   
            var line1_data = dataset.line1;
            drawLineChart($('#line1'),line1_data.labels,line1_data.data,
                                   "Daily Attendance");
        },'json');
        
   
</script>

@endsection


