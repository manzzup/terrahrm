<!DOCTYPE html>
<html lang="en">
    <head>
        <title>TerraHRM</title>
        <script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/datepicker/moment.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/datepicker/bootstrap-datetimepicker.min.js') }}"></script>
        <link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/datepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
    </head>

    <body>
@yield('content')
    </body>
</html>