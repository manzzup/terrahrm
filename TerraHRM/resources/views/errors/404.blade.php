@extends('layouts.app')

@section('content')
<style>
    .sidebar{
        display:none
    }
    body{
        overflow:hidden;
        margin:0px;
        padding:0px;
        padding-top:70px;
    }
</style>
<script>
    $('#content-main').attr('class','col-xs-12 main');
</script>
<div>
    <h1>The page you requested doesn't seem to exist!</h1>
</div>
@endsection