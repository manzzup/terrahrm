<!-- resources/views/layouts/app.blade.php -->

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>TerraHRM</title>
        <script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/datepicker/moment.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/datepicker/bootstrap-datetimepicker.min.js') }}"></script>
        
        <link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/datepicker/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
        <!-- CSS And JavaScript -->
    </head>
    <body>
        
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Terra HRM</a>
                  </div>
                  <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                      <li class="{{ isActive('dashboard') }}"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                      
                      @can('access_admin_module',getAuthUser())
                      <li class="dropdown {{ isActive('admin') }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          @can('access_admin_user',getAuthUser())<li><a href="{{ route('admin.user.viewUsers') }}">Users</a></li>@endcan
                          @can('access_admin_pim',getAuthUser())<li><a href="{{ route('admin.pim.viewDepartments') }}">PIM</a></li>@endcan
                          @can('access_admin_leave',getAuthUser())<li><a href="{{ route('admin.leave.viewLeavePeriods') }}">Leave</a></li>@endcan
                          <!--<li><a href="#">Payroll</a></li>-->
                        </ul>
                      </li>
                      @endcan
                      
                      @can('access_pim_module',getAuthUser())
                      <li class="dropdown {{ isActive('pim') }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PIM <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          @can('access_pim_view',getAuthUser())<li><a href="{{ route('pim.viewEmployee') }}">View Employees</a></li>@endcan
                          @can('access_pim_add',getAuthUser())<li><a href="{{ route('pim.addEmployee') }}"  >Add New Employees</a></li>@endcan
                          <li><a href="{{ route('report.list','pim') }}"  >Reports</a></li>
                        </ul>
                      </li>
                      @endcan
                      
                      @can('access_leave_module',getAuthUser())
                      <li class="dropdown {{ isActive('leave') }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Leave <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          @can('access_leave_history',getAuthUser())<li><a href="{{ route('leave.summary') }}">Leave Summary</a></li>@endcan
                          @can('access_leave_assign',getAuthUser())<li><a href="{{ route('leave.apply') }}">Apply Leave</a></li>@endcan
                          @can('access_leave_entitle',getAuthUser())<li><a href="{{ route('leave.entitlement') }}">Leave Entitlement</a></li>@endcan                         
                          @can('access_leave_balance',getAuthUser())<li><a href="{{ route('leave.balance') }}">Leave Balance</a></li>@endcan  
                          <li><a href="{{ route('report.list','leave') }}"  >Reports</a></li>
                        </ul>
                      </li>
                      @endcan
                      
                      @can('access_attendance_module',getAuthUser())
                      <li class="dropdown {{ isActive('attendance') }}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Attendance <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          @can('access_attendance_upload',getAuthUser())<li><a href="{{ route('attendance.upload') }}">Upload Daily Attendance</a></li>@endcan
                          @can('access_attendance_history',getAuthUser())<li><a href="{{ route('attendance.history') }}">Attendance Summary</a></li>@endcan
                          @can('access_attendance_issues',getAuthUser())<li><a href="{{ route('attendance.issues') }}">Attendance Issues</a></li>@endcan
                          <li><a href="{{ route('report.list','attendance') }}"  >Reports</a></li>                        
                        </ul>
                      </li>
                      @endcan
<!--                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Payroll <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="#">Employee Payroll Summary</a></li>
                          <li><a href="#">Add Transactions</a></li>
                        </ul>
                      </li>-->

                        
                    </ul>
                      
                      <ul class="nav navbar-nav pull-right">
                          <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ getAuthUserName() }} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <li><a href="{{ route('logout') }}">Logout</a></li>
                            </ul>
                        </li>
                          
                      </ul>
                    
                  </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
              </nav>
        
        
        
        
        <div class="col-xs-2 sidebar">
            
            @yield('sidebar')
            
        </div>
        <div class="col-xs-offset-2 col-xs-10 main " id="content-main">
            @if (isset($errors) && count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (!empty($success))
                <div class="alert alert-success">
                    
                        
                            <p>{{ $success }}</p>
                        
                    
                </div>
            @endif
            
                
            
            @yield('content')
            
        </div>
           
        
    </body>
</html>