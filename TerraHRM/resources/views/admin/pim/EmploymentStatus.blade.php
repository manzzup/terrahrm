<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
      <li><a href="{{ route('admin.pim.viewGOC') }}">Group of Companies</a></li>
      <li><a href="{{ route('admin.pim.viewDepartments') }}">Departments</a></li>
      <li><a href="{{ route('admin.pim.viewJobCategories') }}">Job Categories</a></li>
      <li class='active'><a href="{{ route('admin.pim.viewEmploymentStatuses') }}">Employment Statuses</a></li>
    </ul>

@endsection

@section('content')

<h2>Employment Status</h2>
<hr>
 
<form class="form-horizontal" role="form" action="{{ route('admin.pim.addEmploymentStatuses') }}" method='post'>
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <div class="form-group">
        
        <label class="control-label col-sm-3" for="searchBy">Employment Status Name:</label>
        <div class="col-sm-2">
            <input type="text" class="form-control" id="statusName" name="statusName" placeholder="Status Name" value="{{ old('statusName') }}" autofocus>
        </div>    
        
        <div class="col-sm-2">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
      
    </div>   
    
</form>


<br><br>


  <table class="table table-bordered table table-striped">
    <thead>
      <tr>
        
        <th>Employment Status</th>
         <th>Action</th>
       
      </tr>
    </thead>
       <tbody>
           @if(isset($statuses))
                @foreach($statuses as $d)
                    <tr>
                        
                        <td>{{ $d->title }}</td>
                        <td><a href="{{ route('admin.pim.removeEmploymentStatuses',$d->id) }}"  onclick="return confirm('Confirm removing Employment Status')">Remove</a>
                      </tr>
                     
                @endforeach
            @else 
               
            @endif

      
      
    </tbody
  </table>

    
     
    
@endsection


   
