<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
      <li><a href="{{ route('admin.pim.viewGOC') }}">Group of Companies</a></li>
      <li><a href="{{ route('admin.pim.viewDepartments') }}">Departments</a></li>
      <li class='active'><a href="{{ route('admin.pim.viewJobCategories') }}">Job Categories</a></li>
      <li><a href="{{ route('admin.pim.viewEmploymentStatuses') }}">Employment Statuses</a></li>
    </ul>

@endsection

@section('content')

<h2>Job Categories</h2>
<hr>
 
<form class="form-horizontal" role="form" action="{{ route('admin.pim.addJobCategories') }}" method='post'>
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <div class="form-group">
        
        <label class="control-label col-sm-2" for="searchBy">Job Category Name:</label>
        <div class="col-sm-2">
            <input type="text" class="form-control" id="jobName" name="jobName" placeholder="Category Name" value="{{ old('jobName') }}" autofocus>
        </div>    
        
        <div class="col-sm-2">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
      
    </div>   
    
</form>


<br><br>


  <table class="table table-bordered table table-striped">
    <thead>
      <tr>
        
        <th>Job Category</th>
         <th>Action</th>
       
      </tr>
    </thead>
       <tbody>
           @if(isset($jobs))
                @foreach($jobs as $d)
                    <tr>
                        
                        <td>{{ $d->category_name }}</td>
                        <td><a href="{{ route('admin.pim.removeJobCategories',$d->id) }}"  onclick="return confirm('Confirm removing Job Category')">Remove</a>
                      </tr>
                     
                @endforeach
            @else 
               
            @endif

      
      
    </tbody
  </table>

    
     
    
@endsection


   
