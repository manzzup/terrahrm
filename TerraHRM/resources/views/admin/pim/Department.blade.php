<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
      <li><a href="{{ route('admin.pim.viewGOC') }}">Group of Companies</a></li>
      <li class='active'><a href="{{ route('admin.pim.viewDepartments') }}">Departments</a></li>
      <li><a href="{{ route('admin.pim.viewJobCategories') }}">Job Categories</a></li>
      <li><a href="{{ route('admin.pim.viewEmploymentStatuses') }}">Employment Statuses</a></li>
    </ul>

@endsection

@section('content')
    
<h2>Departments</h2>
<hr>

<form class="form-horizontal" role="form" action="{{ route('admin.pim.addDepartment') }}" method='post'>
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <div class="form-group">
        
        <label class="control-label col-sm-2" for="searchBy">Department Name:</label>
        <div class="col-sm-2">
            <input type="text" class="form-control" id="dptName" name="dptName" placeholder="Department Name" value="{{ old('dptName') }}" autofocus>
        </div>    
        
        <div class="col-sm-2">
            <button type="submit" class="btn btn-default">Add</button>
        </div>
      
    </div>   
    
</form>


<br><br>


  <table class="table table-bordered table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Department Name</th>
         <th>Action</th>
       
      </tr>
    </thead>
       <tbody>
           @if(isset($dpts))
                @foreach($dpts as $d)
                    <tr>
                        <td>{{ $d->id }}</td>
                        <td>{{ $d->name }}</td>
                        <td><a href="{{ route('admin.pim.removeDepartment',$d->id) }}" onclick="return confirm('Confirm removing Department')">Remove</a>
                      </tr>
                     
                @endforeach
            @else 
               
            @endif

      
      
    </tbody
  </table>

    
     
    
@endsection


   
