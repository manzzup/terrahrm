<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
      <li class='active'><a href="{{ route('admin.pim.viewGOC') }}">Group of Companies</a></li>
      <li><a href="{{ route('admin.pim.viewDepartments') }}">Departments</a></li>
      <li><a href="{{ route('admin.pim.viewJobCategories') }}">Job Categories</a></li>
      <li><a href="{{ route('admin.pim.viewEmploymentStatuses') }}">Employment Statuses</a></li>
    </ul>

@endsection

@section('content')

<h2>Group of Company</h2>
<hr>
 
<form class="form-horizontal" role="form" action="{{ route('admin.pim.addGOC') }}" method='post'>
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <div class="form-group">
        
        <label class="control-label col-sm-2" for="searchBy">Group of Company:</label>
        <div class="col-sm-2">
            <input type="text" class="form-control" id="gocName" name="gocName" placeholder="Group of Company" value="{{ old('gocName') }}" autofocus>
        </div>    
        
        <div class="col-sm-2">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
      
    </div>   
    
</form>


<br><br>


  <table class="table table-bordered table table-striped">
    <thead>
      <tr>
        
        <th>Group of Company</th>
         <th>Action</th>
       
      </tr>
    </thead>
       <tbody>
           @if(isset($gocs))
                @foreach($gocs as $d)
                    <tr>
                        
                        <td>{{ $d->name }}</td>
                        <td><a href="{{ route('admin.pim.removeGOC',$d->id) }}"  onclick="return confirm('Confirm removing Group of Company')">Remove</a>
                      </tr>
                     
                @endforeach
            @else 
               
            @endif

      
      
    </tbody
  </table>

    
     
    
@endsection


   
