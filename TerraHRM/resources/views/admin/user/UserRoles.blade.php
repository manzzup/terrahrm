<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
      <li><a href="{{ route('admin.user.viewUsers') }}">Users</a></li>
      <li class='active'><a href="{{ route('admin.user.viewUserRoles') }}">User Roles</a></li>
      <li><a href="{{ route('admin.user.viewAssignPerm') }}">Assign Role Permissions</a></li>      
    </ul>

@endsection

@section('content')

<h2>User Roles</h2>
<hr>
 
<form class="form-horizontal" role="form" action="{{ route('admin.user.addUserRole') }}" method='post'>
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <div class="form-group">
        
        <label class="control-label col-sm-2" for="searchBy">User Role:</label>
        <div class="col-sm-2">
            <input type="text" class="form-control" id="roleName" name="roleName" placeholder="Role Name" value="{{ old('roleName') }}" autofocus>
        </div>    
        
        <div class="col-sm-2">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
      
    </div>   
    
</form>


<br><br>


  <table class="table table-bordered table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>User Role</th>
         <th>Action</th>
       
      </tr>
    </thead>
       <tbody>
           @if(isset($roles))
                @foreach($roles as $d)
                    <tr>
                        <td>{{ $d->role_id }}</td>
                        <td>{{ $d->role_name }}</td>
                        <td><a href="{{ route('admin.user.removeUserRole',$d->role_id) }}">Remove</a>
                      </tr>
                     
                @endforeach
            @else 
               
            @endif

      
      
    </tbody
  </table>

    
     
    
@endsection


   
