<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
      <li><a href="{{ route('admin.user.viewUsers') }}">Users</a></li>
      <li><a href="{{ route('admin.user.viewUserRoles') }}">User Roles</a></li>
      <li class='active'><a href="{{ route('admin.user.viewAssignPerm') }}">Assign Role Permissions</a></li>      
    </ul>

@endsection

@section('content')

<h2>Assign Permission to User Role</h2>
<hr>
 
<form class="form-horizontal" role="form" action="{{ route('admin.user.viewAssignPerm') }}" method='get'>
    
    <div class="form-group">
        
        <label class="control-label col-sm-2" for="searchBy">Select User Role:</label>
        <div class="col-sm-2">
            <select class="form-control" name="role">
                <option value="">Select</option>
                    @if(isset($roles))
                        @foreach($roles as $r)
                            @if(app('request')->input('role') !== null && app('request')->input('role') == $r->role_id)
                                <option value="{{ $r->role_id }}" selected>{{$r->role_name}}</option>
                            @else
                                <option value="{{ $r->role_id }}">{{$r->role_name}}</option>
                            @endif
                       @endforeach
                    @endif
            </select>
        </div>   
        
        <div class="col-sm-2">
            <button type="submit" class="btn btn-primary">Select</button>
        </div>
      
    </div>   
    
</form>


<br><br>

@if(isset($perms))
    <h4>Set Permissions</h3>
    <br>
    <form class="form-horizontal" role="form" action="{{ route('admin.user.assignPerm') }}" method='post'>
       <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
        <input type="hidden" value="{{app('request')->input('role')}}" name="role" />
    <div class="col-xs-12">
        @foreach($perms as $p)
            @if($p->parent !== null)
                <div class="form-group">
                    <div class="col-xs-4 @if($p->parent !== 0) col-xs-offset-1 @endif">
                        <input value="{{$p->perm_id}}" name="perm_group[]" data-id="{{$p->perm_id}}" data-parent="{{$p->parent}}" type="checkbox" @if(in_array($p->perm_id,$sel_perms)) checked="checked" @endif /> {{$p->perm_name}}
                    </div>
                </div>
            @endif
        @endforeach
    </div>
        <hr align=center size=2>
        <div class="form-group">  
            <div class="col-sm-2">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </div>
    </form>
    
    <script type="text/javascript">
        $('input[type=checkbox]').change(function(e){
            var id = $(this).data('id');
            var parent = $(this).data('parent');
            
            if(parent === 0){
                if($(this).is(':checked')){
                    $('[data-parent='+id+']').prop('checked',true);
                }else{
                    $('[data-parent='+id+']').prop('checked',false);
                }
            }else{
                if($(this).is(':checked')){
                    $('[data-id='+parent+']').prop('checked',true);
                }
            }
        });
    </script>

@endif

  
    
     
    
@endsection


   
