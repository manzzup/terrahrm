<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
      <li class='active'><a href="{{ route('admin.user.viewUsers') }}">Users</a></li>
      <li><a href="{{ route('admin.user.viewUserRoles') }}">User Roles</a></li>
      <li><a href="{{ route('admin.user.viewAssignPerm') }}">Assign Role Permissions</a></li>      
    </ul>

@endsection

@section('content')

<h2>Users</h2>
<hr>
 
<form class="form-horizontal" role="form" action="{{ route('admin.user.addUser') }}" method='post'>
    <h4>Add New User</h4>
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
   
    <div class="form-group">        
        <label class="control-label col-sm-2" for="username">Username:</label>
        <div class="col-sm-2">
            <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="{{ old('username') }}" autofocus>
        </div>    
    </div>
    <div class="form-group">        
        <label class="control-label col-sm-2" for="password">Password:</label>
        <div class="col-sm-2">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" autofocus>
        </div>  
        
        <label class="control-label col-sm-2" for="password_confirmation">Confirm Password:</label>
        <div class="col-sm-2">
            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password" value="" autofocus>
        </div>  
    </div>
    <div class="form-group">        
        <label class="control-label col-sm-2" for="username">Employee:</label>
        <div class="col-sm-3">
            <input type="text" class="form-control" id="empName" name="empName" readonly autofocus>
            <input type="hidden" name="empID" id='empID' />
        </div>   
        <div class="col-sm-2">
            <button type="button" class="btn btn-default" onclick="openSearchEmployeeDialog('{{ route('pim.viewEmployee') }}?select=true',addUserCallback)">Select</button>
        </div>
    </div>
    <div class="form-group">        
        <label class="control-label col-sm-2" for="role">User Role:</label>
        <div class="col-sm-2">
            <select class="form-control" name="role">
                <option value="">Select</option>
                    @if(isset($roles))
                        @foreach($roles as $r)
                           <option value="{{ $r->role_id }}">{{$r->role_name}}</option>

                       @endforeach
                    @endif
            </select>
        </div>    
    </div>
    <hr  align=center size=2>
    <div class="form-group">  
        <div class="col-sm-2">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
    </div>
      
     
    
</form>


<br><br>

    <h4>User List</h4>
  <table class="table table-bordered table table-striped">
    <thead>
      <tr>
        <th>Username</th>
        <th>User Role</th>
        <th>Employee ID</th>
        <th>Employee Name</th> 
        <th>Action</th> 
      </tr>
    </thead>
       <tbody>
           @if(isset($users))
                @foreach($users as $u)
                    <tr>
                        <td>{{ $u->username }}</td>
                        <td>{{ $u->role->role_name }}</td>
                        <td>{{ $u->employee->emp_id }}</td>
                        <td>{{ $u->employee->first_name }}</td>
                        <td><a href="{{ route('admin.user.removeUser',$u->id) }}">Archive</a>
                      </tr>
                     
                @endforeach
            @else 
               
            @endif

      
      
    </tbody
  </table>

<script type="text/javascript" src="{{ asset('js/searchEmployee.js') }}"></script>

     
    
@endsection


   
