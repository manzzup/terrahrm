<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
      <li><a href="{{ route('admin.leave.viewLeavePeriods') }}">Leave Periods</a></li>
      <li class="active"><a href="{{ route('admin.leave.viewLeaveTypes') }}">Leave Types</a></li>
    </ul>

@endsection

@section('content')

<h2>Leave Types</h2>
<hr>
 
<form class="form-horizontal" role="form" action="{{ route('admin.leave.addLeaveType') }}" method='post'>
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <div class="form-group">
        
        <label class="control-label col-sm-2" for="searchBy">Leave Type:</label>
        <div class="col-sm-2">
            <input type="text" class="form-control" id="leaveType" name="leaveType" placeholder="Leave Type" value="{{ old('leaveType') }}" autofocus>
        </div>    
        
        <div class="col-sm-2">
            <button type="submit" class="btn btn-default">Add</button>
        </div>
      
    </div>   
    
</form>


<br><br>


  <table class="table table-bordered table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Leave Type Title</th>
         <th>Action</th>
       
      </tr>
    </thead>
       <tbody>
           @if(isset($lvs))
                @foreach($lvs as $l)
                    <tr>
                        <td>{{ $l->id }}</td>
                        <td>{{ $l->leave_title }}</td>
                        <td><a href="{{ route('admin.leave.removeLeaveType',$l->id) }}"  onclick="return confirm('Confirm removing Leave Type')">Remove</a>
                      </tr>
                     
                @endforeach
            @else 
               
            @endif

      
      
    </tbody
  </table>

    
     
    
@endsection


   
