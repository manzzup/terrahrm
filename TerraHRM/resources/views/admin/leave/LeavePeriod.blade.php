<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
      <li class="active"><a href="{{ route('admin.leave.viewLeavePeriods') }}">Leave Periods</a></li>
      <li><a href="{{ route('admin.leave.viewLeaveTypes') }}">Leave Types</a></li>
    </ul>

@endsection

@section('content')

<h2>Leave Periods</h2>
<hr>
 
<form class="form-horizontal" role="form" action="{{ route('admin.leave.addLeavePeriod') }}" method='post'>
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    
    <div class="form-group">
    <label class="control-label col-sm-2" for="searchBy">Leave year:</label>
        <div class="col-sm-2">
            <input type="number" class="form-control" id="leaveYear" name="leaveYear" placeholder="Leave Year" value="{{ old('leaveYear') }}" autofocus>
        </div> 
    </div>
    
    <div class="form-group">       
        <label class="control-label col-sm-2" for="fromDate">From :</label>
                <div class="col-sm-3">
                    <div class='input-group date' id='fromDate'>
                        <input type='text' class="form-control" name="startDate" value="{{ old('startDate') }}" />
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>  
            
        
        
                <label class="control-label col-sm-2" for="fromDate">To :</label>
                <div class="col-sm-3">
                    <div class='input-group date' id='toDate'>
                        <input type='text' class="form-control" name="endDate" value="{{old('endDate')}}"/>
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div> 
        
        
      
    </div>
    
    <div class="col-xs-offset-2 col-sm-2">
            <button type="submit" class="btn btn-default">Add</button>
        </div>
    
</form>


<br><br><br>


  <table class="table table-bordered table table-striped">
    <thead>
      <tr>
        <th>Leave Year</th>
        <th>Start Date</th>
        <th>End Date</th>
         <th>Action</th>
       
      </tr>
    </thead>
       <tbody>
           @if(isset($lvs))
                @foreach($lvs as $l)
                    <tr>
                        <td>{{ $l->leave_year }}</td>
                        <td>{{ $l->from_date }}</td>
                        <td>{{ $l->to_date }}</td>
                        <td><a href="{{ route('admin.leave.removeLeavePeriod',$l->id) }}"  onclick="return confirm('Confirm removing Leave Period')">Remove</a>
                      </tr>
                     
                @endforeach
            @else 
               
            @endif

      
      
    </tbody
  </table>
<script type="text/javascript">
    $(function () {
        $('#fromDate').datetimepicker({
            format: 'YYYY-MM-DD',
            defaultDate: 'now'
        });
    });
    $(function () {
        $('#toDate').datetimepicker({
            format: 'YYYY-MM-DD',
            defaultDate: 'now'
        });
    });
</script>  
    
@endsection


   
