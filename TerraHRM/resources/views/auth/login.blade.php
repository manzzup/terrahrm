<!-- resources/views/auth/login.blade.php -->

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>TerraHRM</title>
        <script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        <link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
        
        <style>
            body {
                background: #e9e9e9;
                color: #666666;
                font-family: 'RobotoDraft', 'Roboto', sans-serif;
                font-size: 14px;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
              }

              /* Pen Title */
              .pen-title {
                padding: 50px 0;
                text-align: center;
                letter-spacing: 2px;
              }
              .pen-title h1 {
                margin: 0 0 20px;
                font-size: 48px;
                font-weight: 300;
              }
              .pen-title span {
                font-size: 12px;
              }
              .pen-title span .fa {
                color: #33b5e5;
              }
              .pen-title span a {
                color: #33b5e5;
                font-weight: 600;
                text-decoration: none;
              }

              /* Form Module */
              .form-module {
                position: relative;
                background: #ffffff;
                max-width: 320px;
                width: 100%;
                border-top: 5px solid #5c2040;
                box-shadow: 0 0 3px rgba(0, 0, 0, 0.1);
                margin: 0 auto;
              }
              .form-module .toggle {
                cursor: pointer;
                position: absolute;
                top: -0;
                right: -0;
                background: #5c2040;
                width: 30px;
                height: 30px;
                margin: -5px 0 0;
                color: #ffffff;
                font-size: 12px;
                line-height: 30px;
                text-align: center;
              }
              .form-module .toggle .tooltip {
                position: absolute;
                top: 5px;
                right: -65px;
                display: block;
                background: rgba(0, 0, 0, 0.6);
                width: auto;
                padding: 5px;
                font-size: 10px;
                line-height: 1;
                text-transform: uppercase;
              }
              .form-module .toggle .tooltip:before {
                content: '';
                position: absolute;
                top: 5px;
                left: -5px;
                display: block;
                border-top: 5px solid transparent;
                border-bottom: 5px solid transparent;
                border-right: 5px solid rgba(0, 0, 0, 0.6);
              }
              .form-module .form {
                display: none;
                padding: 40px;
              }
              .form-module .form:nth-child(2) {
                display: block;
              }
              .form-module h2 {
                margin: 0 0 20px;
                color: #5c2040;
                font-size: 18px;
                font-weight: 400;
                line-height: 1;
              }
              .form-module input {
                outline: none;
                display: block;
                width: 100%;
                border: 1px solid #d9d9d9;
                margin: 0 0 20px;
                padding: 10px 15px;
                box-sizing: border-box;
                font-wieght: 400;
                -webkit-transition: 0.3s ease;
                transition: 0.3s ease;
              }
              .form-module input:focus {
                border: 1px solid #dd4814;
                color: #333333;
              }
              .form-module button {
                cursor: pointer;
                background: #dd4814;
                width: 100%;
                border: 0;
                padding: 10px 15px;
                color: #ffffff;
                -webkit-transition: 0.3s ease;
                transition: 0.3s ease;
              }
              .form-module button:hover {
                background: #ae3910;
              }
              .form-module .cta {
                background: #f2f2f2;
                width: 100%;
                padding: 15px 40px;
                box-sizing: border-box;
                color: #666666;
                font-size: 12px;
                text-align: center;
              }
              .form-module .cta a {
                color: #333333;
                text-decoration: none;
              }
              
              
        </style>
    
    </head>

    <body>
        <div class="container">
<!--            <form  class="form-horizontal" role="form"   method="POST" action="login">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label class=control-label" for="username">Username</label>
                    <input type="text" class="form-control" name="username" value="{{ old('username') }}">
                </div>

                <div class="form-group">

                   <label class=control-label" for="password">Password</label>
                   <input type="password" class="form-control" name="password" id="password">
                </div>

                <div>
                    <input type="checkbox" name="remember"> Remember Me
                </div>

                <div>
                    <button type="submit">Login</button>
                </div>
            </form>-->
            <div class="pen-title">
                <h1>Terra HRM</h1>
              </div>
            <div class="module form-module">
              <div class="toggle"><i class="fa fa-times fa-pencil"></i>
                <div class="tooltip">Click Me</div>
              </div>
              <div class="form">
                <h2>Login to your account</h2>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form  method="POST" action="login">
                    {!! csrf_field() !!}
                  <input type="text" placeholder="Username"  name="username" value="{{ old('username') }}" />
                  <input type="password" placeholder="Password"  name="password" id="password" />
                  <input type="checkbox" name="remember" style="width:auto;display: inline-block"> Remember Me
                  <button>Login</button>
                </form>
              </div>
        </div>
    </body>
</html>
       
