@extends('layouts.app')

@section('content')
<style>
    .sidebar{
        display:none
    }
    body{
        overflow:hidden;
        margin:0px;
        padding:0px;
        padding-top:70px;
    }
</style>
<script>
    $('#content-main').attr('class','col-xs-12 main');
</script>
<div style="height:100vh;overflow: hidden">
    <iframe src="{{ $link }}" width="100%" height="100%" frameborder="0" scrolling="yes" style="height:100vh; overflow:scroll"></iframe>
</div>
@endsection