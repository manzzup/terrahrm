<h2>Reports</h2>
<hr>
@if (isset($reports))
<ul>
    @foreach ($reports as $r)
        <li><a href="{{ route('report.view',$r->id) }}">{{$r->title}}</li>
    @endforeach    
</ul>
@endif