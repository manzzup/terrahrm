

@extends('layouts.app')
@section('sidebar')
    <h4 align="center">{{ $employee->first_name }} {{ $employee->last_name }}</h4>
    <hr  align=center size=2>
            
    <ul class="nav nav-sidebar">
      <li><a href="{{ route('pim.viewPersonalDetails',$employee->id) }}">Personal Details</a></li>
      <li><a href="{{ route('pim.viewContactDetails',$employee->id) }}">Contact Details</a></li>
      <li class='active'><a href="{{ route('pim.viewEmploymentDetails',$employee->id) }}">Employment Details</a></li>
      
    </ul>

@endsection
@section('content')
    
    
  <div class="row">
      
    <div class="col-xs-12">
         <h2>Employment Details</h2>
            <hr> 
        <form class="form-horizontal" role="form" action='{{ route('pim.updateEmploymentDetails',$employee->id) }}' method='post'>
          
            
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
            <div class="form-group">
                <label class="control-label col-sm-2" for="goc">Group of Companies:</label>
                <div class="col-sm-2"> 
                    <select class="form-control" id="goc" name="goc" value="{{$employment_detail->group_of_companies}}">

                        @if(isset($goc))
                        
                            @foreach($goc as $c)
                                @if( old('goc')!=null)
                                    @if(old('goc')==$c->id)
                                        <option value="{{$c->id}}" selected>{{$c->name}}</option>
                                    @else
                                        <option value="{{$c->id}}">{{$c->name}}</option>
                                    @endif
                                @else  
                                    @if($employment_detail->group_of_companies==$c->id)
                                        <option value="{{$c->id}}" selected>{{$c->name}}</option>
                                    @else
                                        <option value="{{$c->id}}" >{{$c->name}}</option>      
                                    @endif
                                @endif
                        
                        

                          @endforeach
                        @else 

                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="department">Department:</label>
                <div class="col-sm-2">
                    <select class="form-control" id="department" name="department" value="{{$employment_detail->department}}">
                        @if(isset($departments))
                        
                            @foreach($departments as $d)
                                @if( old('department')!=null)
                                    @if(old('department')==$d->id)
                                        <option value="{{$d->id}}" selected>{{$d->name}}</option>
                                    @else
                                        <option value="{{$d->id}}">{{$d->name}}</option>
                                    @endif
                                @else  
                                    @if($employment_detail->department==$d->id)
                                        <option value="{{$d->id}}" selected>{{$d->name}}</option>
                                    @else
                                        <option value="{{$d->id}}" >{{$d->name}}</option>      
                                    @endif
                                @endif
                        
                        
                          
                          @endforeach
                        @else 

                        @endif
                       
                    </select>    
                </div>    
            </div>
            <div class="form-group required">
                <label class="control-label col-sm-2" for="jobTitle">Job Title:</label>
                 <div class="col-sm-2">
                    @if(old('jobTitle')!=null)
                        <input type="text" class="form-control" id="jobTitle" name="jobTitle" value="{{old('jobTitle')}}" >
                    @else
                        <input type="text" class="form-control" id="jobTitle" name="jobTitle" value="{{$employment_detail->job_title}}" >
                    @endif
                   
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="jobCategory">Job Category:</label>
                <div class="col-sm-2">
                     <select class="form-control" id="jobCategory" name="jobCategory" value="{{$employment_detail->job_category}}">
                         
                          @if(isset($job_categories))
                        
                            @foreach($job_categories as $j)
                               
                                @if( old('jobCategory')!=null)
                                    @if(old('jobCategory')==$j->id)
                                        <option value="{{$j->id}}" selected>{{$j->category_name}}</option>
                                    @else
                                        <option value="{{$j->id}}">{{$j->category_name}}</option>
                                    @endif
                                @else  
                                    @if($employment_detail->job_category==$j->id)
                                        <option value="{{$j->id}}" selected>{{$j->category_name}}</option>
                                    @else
                                        <option value="{{$j->id}}" >{{$j->category_name}}</option>      
                                    @endif
                                @endif
                        
                        

                          @endforeach
                        @else 

                        @endif
                    </select>    
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="empStatus">Employment Status:</label>
                <div class="col-sm-2">
                     <select class="form-control" id="empStatus" name="empStatus" value="{{$employment_detail->employment_status}}">
                        
                        @if(isset($employment_status))
                        
                            @foreach($employment_status as $s)
                                 @if( old('empStatus')!=null)
                                    @if(old('empStatus')==$s->id)
                                        <option value="{{$s->id}}" selected>{{$s->title}}</option>
                                    @else
                                        <option value="{{$s->id}}">{{$s->title}}</option>
                                    @endif
                                @else  
                                    @if($employment_detail->employment_status==$s->id)
                                        <option value="{{$s->id}}" selected>{{$s->title}}</option>
                                    @else
                                        <option value="{{$s->id}}" >{{$s->title}}</option>      
                                    @endif
                                @endif
                                
                       

                          @endforeach
                        @else 

                        @endif
                    </select> 
                </div>
            </div>
             <div class="form-group required">
                <label class="control-label col-sm-2" for="joinedDate">Joined Date:</label>
                <div class="col-sm-2">
                   
                       <div class='input-group date' id='datetimepicker'>
                             @if(old('joinedDate')!=null)
                                <input type='text' id='joinedDate' name='joinedDate' class="form-control" value="{{old('joinedDate')}}" />
                                
                             @else
                                 <input type='text' id='joinedDate' name='joinedDate' class="form-control" value="{{$employment_detail->joined_date}}" />
                            @endif
                             
                             
                            <span class="input-group-addon">
                             <span class="glyphicon glyphicon-calendar"></span>
                             </span>
                       </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="leftDate">Left Date:</label>
                <div class="col-sm-2">
                    <div class='input-group date' id='datetimepicker2'>
                            @if(old('leftDate')!=null)
                                <input type='text' id='leftDate' name='leftDate' class="form-control" value="{{old('leftDate')}}" /> 
                             @else
                                 <input type='text' id='leftDate' name='leftDate' class="form-control" value="{{$employment_detail->left_date}}" />
                            @endif
                        
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>  
            </div>
            <div class="form-group">
                <label  class="control-label col-sm-2" for="leavingReason">Leaving Reason:</label>
                <div class="col-sm-4">
                     @if(old('leavingReason')!=null)
                        <textarea class="form-control" rows="2" id="leavingReason" name="leavingReason" value="{{old('leavingReason')}}"></textarea>  
                     @else
                         <textarea class="form-control" rows="2" id="leavingReason" name="leavingReason" value="{{$employment_detail->leaving_reason}}"></textarea>  
                     @endif
                   
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="employeeStatus">Employee Status:</label>
                    <div class="col-sm-2">
                    <div class="radio-inline">
                        <label><input type="radio" name="employeeStatus" value="0" <?php echo ($employment_detail->hr_active_flag=='0')?'checked':'' ?> checked="checked" >Active</label>
                    </div>
                     </div>
                    <div class="col-sm-2">
                    <div class="radio-inline" >
                        <label><input type="radio" name="employeeStatus" value="1" <?php echo ($employment_detail->hr_active_flag=='1')?'checked':'' ?>>Inactive</label>
                    </div>
                    </div>
                
            </div>
               <hr  align=center size=2>
            <div class="form-group">
                <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
             
       </form>    
         
    </div>
  </div>
 
<script type="text/javascript">
    $(function () {
        $('#joinedDate').datetimepicker({
            format: 'YYYY-MM-DD',
            
        });
    });
    $(function () {
        $('#leftDate').datetimepicker({
            format: 'YYYY-MM-DD',
         
        });
    });
</script>

    
@endsection
