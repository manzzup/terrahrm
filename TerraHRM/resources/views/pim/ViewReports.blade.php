<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
      <li><a href="{{ route('pim.addEmployee') }}">Add Employee</a></li>
      <li><a href="{{ route('pim.viewEmployee') }}">View Employees</a></li>
      <li class="active"><a href="{{ route('report.list','pim') }}">Reports</a></li>
    </ul>

@endsection

@section('content')    
    
  <div class="row">
      
    <div class="col-xs-12">
       
        @include('common.listReports')
    
    </div>
  </div>


    
@endsection