<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')
    <h4 align="center">{{ $employee->first_name }} {{ $employee->last_name }}</h4>
    <hr  align=center size=2>
    <ul class="nav nav-sidebar">
      <li><a href="{{ route('pim.viewPersonalDetails',$employee->id) }}">Personal Details</a></li>
      <li class='active'><a href="{{ route('pim.viewContactDetails',$employee->id) }}">Contact Details</a></li>
      <li><a href="{{ route('pim.viewEmploymentDetails',$employee->id) }}">Employment Details</a></li>
      
    </ul>

@endsection
@section('content')
    
  
  <div class="row">
      
    <div class="col-sm-12">
       
        <form class="form-horizontal" role="form"  action="{{ route('pim.updateContactDetails',$employee->id) }}" method="post">
            
            <h2>Contact Details</h2>
            <hr>
             @if(isset($contact_detail))
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <div class="col-sm-6">
            
            <div class="form-group">
                
                <label class="control-label col-sm-4" >Address:</label>
            </div>

            
                
                    <div class="form-group required">
                        <div class="col-sm-2">
                        </div>
                        <label class="control-label col-sm-4" for="line1">Line 1:</label>
                        <div class="col-sm-4">
                             @if(old('line1')!=null)
                                 <input type="text" class="form-control" id="line1" name="line1"  value="{{old('line1')}}">
                             @else
                               <input type="text" class="form-control" id="line1" name= "line1"  value="{{$contact_detail->add_line_1}}">
                              @endif
                        </div>
                             
                            
                    </div>
                     <div class="form-group required">
                        <div class="col-sm-2">
                        </div>
                        <label class="control-label col-sm-4" for="line2">Line 2:</label>
                        <div class="col-sm-4">
                             @if(old('line2')!=null)
                                 <input type="text" class="form-control" id="line2" name="line2"  value="{{old('line2')}}">
                             @else
                                <input type="text" class="form-control" id="line2" name= "line2" value="{{$contact_detail->add_line_2}}">
                              @endif
                           
                        </div>
                    </div>
                  <div class="form-group required">
                        <div class="col-sm-2">
                        </div>
                        <label class="control-label col-sm-4" for="city">City:</label>
                        <div class="col-sm-4">
                               @if(old('city')!=null)
                                 <input type="text" class="form-control" id="city" name="city"  value="{{old('city')}}">
                             @else
                                <input type="text" class="form-control" id="city" name="city" value="{{$contact_detail->city}}">
                              @endif
                            
                        </div>
                    </div>
                  <div class="form-group required">
                        <div class="col-sm-2">
                        </div>
                        <label class="control-label col-sm-4" for="province">Province:</label>
                        <div class="col-sm-4">
                             @if(old('province')!=null)
                                 <input type="text" class="form-control" id="province" name="province"  value="{{old('province')}}">
                             @else
                                <input type="text" class="form-control" id="province" name="province" value="{{$contact_detail->province}}">
                              @endif
                             
                        </div>
                    </div>
            </div>
            <div class="form-group">
                
            </div>
             <hr  align=center size=2>  
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="home">Contact No:</label>
                </div>
                <div class="form-group required">
                            <div class="col-sm-2">
                            </div>
                             <label class="control-label col-sm-4" for="work">Work:</label>
                             <div class="col-sm-4">
                                @if(old('workNo')!=null)
                                    <input type="text" class="form-control" id="workNo" name="workNo"  value="{{old('workNo')}}">
                                @else
                                    <input type="text" class="form-control" id="workNo" name= "workNo" value="{{$contact_detail->work_no}}">
                                @endif
                                
                           </div>
                           
                </div>
                <div class="form-group ">
                           <div class="col-sm-2">
                           </div>
                           <label class="control-label col-sm-4" for="homeNo">Home:</label>
                          
                            <div class="col-sm-4">
                                @if(old('homeNo')!=null)
                                 <input type="text" class="form-control" id="homeNo" name="homeNo"  value="{{old('homeNo')}}">
                             @else
                                 <input type="text" class="form-control" id="homeNo" name= "homeNo" value="{{$contact_detail->home_no}}" >
                              @endif
                               
                            </div>
                         
               </div>    
                <div class="form-group">
                           <div class="col-sm-2">
                           </div>
                           <label class="control-label col-sm-4" for="mobileNo">Mobile:</label>
                           <div class="col-sm-4">
                                   @if(old('mobileNo')!=null)
                                 <input type="text" class="form-control" id="mobileNo" name="mobileNo"  value="{{old('mobileNo')}}">
                                @else
                                <input type="text" class="form-control" id="mobileNo" name= "mobileNo" value="{{$contact_detail->mobile_no}}" >
                                 @endif
                               
                           </div>
                </div>   
            </div>
                
           
               
                
            <div class="col-sm-6">
                <div class="form-group  ">
                    <label class="control-label col-sm-4" for="work">Email:</label>
                </div>
                 <div class="form-group required">
                            <div class="col-sm-2">
                            </div>
                            <label class="control-label col-sm-4" for="workMail">Work:</label>
                            <div class="col-sm-4">
                                  @if(old('workMail')!=null)
                                 <input type="text" class="form-control" id="workMail" name="workMail"  value="{{old('workMail')}}">
                                @else
                                 <input type="email" class="form-control" id="workMail" name= "workMail" value="{{$contact_detail->work_email}}" >
                                 @endif
                               
                               
                            </div>
                </div>
                  <div class="form-group">
                            <div class="col-sm-2">
                            </div>
                            <label class="control-label col-sm-4" for="personalMail">Personal:</label>
                            <div class="col-sm-4">
                                 @if(old('personalMail')!=null)
                                 <input type="text" class="form-control" id="personalMail" name="personalMail"  value="{{old('personalMail')}}">
                                @else
                                  <input type="email" class="form-control" id="personalMail" name= "personalMail" value="{{$contact_detail->personal_email}}" >
                                 @endif
                               
                               
                            </div>
                </div>
                
            </div>
            <div class="form-group">
                
            </div>
             <hr  align=center size=2>
             <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label col-sm-4" for="name">Emergency Contact:</label>
            </div>
                  <div class="form-group required">
                        <div class="col-sm-2">
                        </div>
                        <label class="control-label col-sm-4" for="name">Name:</label>
                        <div class="col-sm-4">
                             @if(old('name')!=null)
                                 <input type="text" class="form-control" id="name" name="name"  value="{{old('name')}}">
                             @else
                                  <input type="text" class="form-control" id="name" name= "name" value="{{$contact_detail->emergency_name}}" >
                             @endif
                            
                        </div>
                    </div>
                 <div class="form-group required">
                        <div class="col-sm-2">
                        </div>
                        <label class="control-label col-sm-4" for="relationship">Relationship:</label>
                        <div class="col-sm-4">
                            @if(old('relationship')!=null)
                                 <input type="text" class="form-control" id="relationship" name="relationship"  value="{{old('relationship')}}">
                             @else
                                  <input type="text" class="form-control" id="relationship" name= "relationship" value="{{$contact_detail->emergency_relationship}}" >
                             @endif
                            
                        </div>
                    </div>
              
                 <div class="form-group required">
                        <div class="col-sm-2">
                        </div>
                        <label class="control-label col-sm-4" for="contactNo">Contact No:</label>
                        <div class="emergerncy_contact_no">
                            <div class="col-sm-4">
                            @if(old('contactNo')!=null)
                                 <input type="text" class="form-control" id="contactNo" name="contactNo"  value="{{old('contactNo')}}">
                             @else
                                   <input type="text" class="form-control" id="contactNo" name= "contactNo" value="{{$contact_detail->emergerncy_contact_no}}" >
                             @endif
                              </div>
                           
                        </div>
                    </div>
             </div>
             <div class="form-group">
             </div>
                    <hr  align=center size=2>
            
            <div class="form-group">
                <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
             @else 
               
            @endif
       </form>     
    
    </div>
  </div>


 @endsection
 
