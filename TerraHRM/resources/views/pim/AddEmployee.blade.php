<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')

    <ul class="nav nav-sidebar">
      <li class="active"><a href="{{ route('pim.addEmployee') }}">Add Employee</a></li>
      <li><a href="{{ route('pim.viewEmployee') }}">View Employees</a></li>
      <li><a href="{{ route('report.list','pim') }}">Reports</a></li>
    </ul>

@endsection

@section('content')    
    
  <div class="row">
      
    <div class="col-xs-12">
        <h2>Add New Employee</h2>
        <hr>
       
        <form class="form-horizontal" role="form" method="POST" action="{{ route('pim.addEmployee') }}">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <div class="form-group">
                <label class="control-label col-sm-2" for="firstName">Employee Name:</label>
                
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="firstName" name="firstName" placeholder="first name" value="{{ old('firstName') }}" autofocus>
                </div>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="last name" value="{{ old('lastName') }}">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="empId">Employee ID:</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="empId" name="empId" placeholder="employee ID" value="{{ old('empId') }}">
                </div>
            </div>
        
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
       </form>     
    
    </div>
  </div>


    
@endsection