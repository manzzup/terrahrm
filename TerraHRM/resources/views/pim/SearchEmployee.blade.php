<!-- resources/views/tasks.blade.php -->


@extends( empty(app('request')->input('select')) ? 'layouts.app' : 'default')
@if(!empty(app('request')->input('select')))
    <style>
        body{
            padding:10px;
            padding-top: 0px !important;
        }
    </style>
@endif
@section('sidebar')

    <ul class="nav nav-sidebar">
      <li><a href="{{ route('pim.addEmployee') }}">Add Employee</a></li>
      <li class="active"><a href="{{ route('pim.viewEmployee') }}">View Employees</a></li>
      <li><a href="{{ route('report.list','pim') }}">Reports</a></li>
    </ul>

@endsection



@section('content')
 <h2>Search Employees</h2>
    <hr>   
 
<form class="form-horizontal" role="form" action="{{ route('pim.searchEmployees') }}@if(!empty(app('request')->input('select')))?select=true @endif " method='post'>
    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
    <div class="form-group">
        
        <label class="control-label col-sm-2" for="searchBy">Search By:</label>
        <div class="col-sm-2">
                        <select class="form-control" id="searchBy" name="searchBy" >
                            @if(isset($searchBy))
                                @if($searchBy==1)
                                    <option value="1" selected>ID</option>
                                    <option value="2">Name</option>
                                    <option value="3">Department</option>
                                @elseif($searchBy==2)
                                    <option value="1" >ID</option>
                                    <option value="2" selected>Name</option>
                                    <option value="3">Department</option>
                                @elseif($searchBy==3)
                                    <option value="1" >ID</option>
                                    <option value="2" >Name</option>
                                    <option value="3" selected>Department</option>
                                @endif
                            @else
                                 <option value="1" >ID</option>
                                <option value="2" >Name</option>
                                <option value="3" >Department</option>
                            
                            @endif
                        </select>
        </div>
        <div class="col-sm-2"  id="query">
             @if(isset($query))
             <input type="text" class="form-control" name="query" value="{{$query}}" >
             @else
                <input type="text" class="form-control" name="query"  autofocus>
             @endif
        </div>
        <div class="col-sm-2"  id="department" style="display: none">
            <select class="form-control" name="department" value="{{ old('department') }}" >
                 @if(isset($departments))
                        
                            @foreach($departments as $d)
                            
                                @if(isset($department))
                            
                                    @if($department==$d->id)
                                        <option value="{{$d->id}}" selected>{{$d->name}}</option>
                                    @else
                                        <option value="{{$d->id}}">{{$d->name}}</option>
                                    @endif
                                @else
                                    <option value="{{$d->id}}">{{$d->name}}</option>
                                @endif
                             

                          @endforeach
                        @else 
                                
                        @endif
                           
            </select>
        </div>
   
        <div class="col-sm-2">
            <button type="submit" class="btn btn-primary">Search</button>
        </div>
        
      
    </div>
    
</form>


    <br><br><br>
    
 @if(isset($employees))
@if(!$employees->isEmpty())


  <table class="table table-bordered table table-striped" id="tblEmp">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
         <th>Department</th>
        <th>Job Title</th>
        <th>Job Category</th>
       
      </tr>
    </thead>
       <tbody>
          
                
                 
                @foreach($employees as $e)
                   
                    
                    <tr>
                        <td><a href="{{ route('pim.viewPersonalDetails',$e->id) }}" data-id="{{ $e->id }}" data-empid="{{ $e->emp_id }}" data-name="{{ $e->first_name }}" @if(!empty(app('request')->input('select'))) onclick="selectEmp(this); return false;" @endif>{{ $e->emp_id }}</a></td>
                        <td>{{ $e->first_name }} {{ $e->last_name }}</td>
                        @if(($e->employment_details)!=null)
                            <td>{{ $e->employment_details->department_obj->name }}</td>
                            <td>{{ $e->employment_details->job_title }}</td>
                            <td>{{ $e->employment_details->job_category_obj->category_name}}</td> 
                        
                        @else
                        <td></td>
                        <td></td>
                         <td></td>
                        @endif
                        
                        
                        
                      </tr>
                     
                @endforeach
            
            
      
      
    </tbody
  </table>
@else
 <div class="alert alert-warning">
     <p>No result found</p>
</div>
@endif
@else

@endif

<script type="text/javascript" src="{{ asset('js/searchEmployee.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#searchBy").trigger('change');
    });
    $("#searchBy").change(function(e){
        if($("#searchBy option:selected").text() === 'Department'){
            $('#query').hide();
            $('#department').show();
        }else{
            $('#query').show();
            $('#department').hide();
        }
    });
</script>
    
     
    
@endsection


   
