<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')
@section('sidebar')
    <h4 align="center">{{ $employee->first_name }} {{ $employee->last_name }}</h4>
    <hr  align=center size=2>
    <ul class="nav nav-sidebar">
      <li class='active'><a href="{{ route('pim.viewPersonalDetails',$employee->id) }}">Personal Details</a></li>
      <li><a href="{{ route('pim.viewContactDetails',$employee->id) }}">Contact Details</a></li>
      <li><a href="{{ route('pim.viewEmploymentDetails',$employee->id) }}">Employment Details</a></li>
      
    </ul>

@endsection

@section('content')
   
    
  <div class="row">
      
    <div class="col-xs-12">
        
        <h2>Personal Details</h2>
        <hr> </hr>
        <form class="form-horizontal" role="form" action="{{ route('pim.updatePersonalDetails',$employee->id) }}" method="post">
            @if(isset($employee))
               
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/> 
             <div class="form-group required">
                
                <label class="control-label col-sm-2" for="fullName">Full Name:</label>
                <div class="col-sm-6">
                    @if(old('fullName')!=null)
                        <input type="text" class="form-control" id="fullName" name="fullName"  value="{{old('fullName')}}">
                    @else
                        <input type="text" class="form-control" id="fullName" name="fullName"  value="{{$employee->full_name}}">
                    @endif
                </div>
            </div>
             <div class="form-group required">
                <label class="control-label col-sm-2" for="firstName">First Name:</label>
                <div class="col-sm-2">
                     @if(old('firstName')!=null)
                        <input type="text" class="form-control" id="firstName" name="firstName"  value="{{old('firstName')}}">
                     @else
                        <input type="text" class="form-control" id="firstName" name="firstName"  value="{{$employee->first_name}}">
                     @endif
                </div>
                <label class="control-label col-sm-2" for="lastName">Last Name:</label>
                <div class="col-sm-2">
                    @if(old('lastName')!=null)
                        <input type="text" class="form-control" id="firstName" name="lastName"  value="{{old('lastName')}}">
                     @else
                         <input type="text" class="form-control" id="lastName" name="lastName"  value="{{$employee->last_name}}">
                     @endif
                   
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="empId" >Employee ID:</label>
                <div class="col-sm-2"> 
                    
                    <input type="text" class="form-control" id="empId" name="empId"  value="{{$employee->emp_id}}" readonly>
                </div>
            </div>
            <hr  align=center size=2>
             <div class="form-group required">
                    <label class="control-label col-sm-2" for="gender">Gender:</label>
                    <div class="col-sm-1">
                        <div class="radio-inline" >
                            <label><input type="radio" name="gender" value="0" <?php echo ($employee->gender=='0')?'checked':'' ?>  >Male</label>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="radio-inline" >
                            <label><input type="radio" name="gender" value="1" <?php echo ($employee->gender=='1')?'checked':'' ?>>Female</label>
                        </div>
                    </div>
            </div>
            <div class="form-group">
                    <label class="control-label col-sm-2" for="maritalStatus">Marital Status:</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="maritalStatus" name="maritalStatus">
                            @if(old('maritalStatus')!=null)
                                @if(old('maritalStatus')==1)
                                          <option value="1" selected>Single</option>
                                          <option value="2">Married</option>
                                        <option value="3">Other</option>
                                @elseif(old('maritalStatus')==2)
                              
                                        <option value="1" >Single</option>
                                          <option value="2"selected>Married</option>
                                        <option value="3">Other</option>
                                @else
                                
                                        <option value="1" >Single</option>
                                          <option value="2">Married</option>
                                        <option value="3"selected>Other</option>
                                @endif
                            @else
                                 @if($employee->marital_status==1)
                                          <option value="1" selected>Single</option>
                                          <option value="2">Married</option>
                                        <option value="3">Other</option>
                                @elseif($employee->marital_status==2)
                              
                                        <option value="1" >Single</option>
                                          <option value="2"selected>Married</option>
                                        <option value="3">Other</option>
                                @else
                                
                                        <option value="1" >Single</option>
                                          <option value="2">Married</option>
                                        <option value="3"selected>Other</option>
                                @endif
                           
                            
                            @endif
                       
                            
                           
                           
                        </select>
                    </div>
            </div>
            <hr  align=center size=2>
            
            <div class="form-group">
                <label class="control-label col-sm-2" for="dateOfBirth">Date of Birth:</label>
                <div class="col-sm-2">
                   
                       <div class='input-group date' id='datetimepicker'>
                             @if(old('dateOfBirth')!=null)
                                <input type='text' id='dateOfBirth' name='dateOfBirth' class="form-control" value="{{old('dateOfBirth')}}" />
                             @else
                                    <input type="text" class="form-control" id="dateOfBirth" name="dateOfBirth"  value="{{$employee->date_of_birth}}">
                            @endif
                             
                            
                            <span class="input-group-addon">
                             <span class="glyphicon glyphicon-calendar"></span>
                             </span>
                       </div>
                </div>
            </div>
             <div class="form-group required">
                
                <label class="control-label col-sm-2 " for="nicNo">NIC No:</label>
                <div class="col-sm-2">
                    @if(old('nicNo')!=null)
                        <input type="text" class="form-control" id="nicNo" name="nicNo"  value="{{old('nicNo')}}">
                     @else
                         <input type="text" class="form-control" id="nicNo" name="nicNo" placeholder="" value="{{$employee->nic}}">
                     @endif
                    
                </div>
             </div>
                <div class="form-group ">
                <label class="control-label col-sm-2" for="license">Driver's License No:</label>
                <div class="col-sm-2">
                     @if(old('driver\'sLicenseNo')!=null)
                        <input type="text" class="form-control" id="driver\'sLicenseNo" name="driver\'sLicenseNo"  value="{{old('driver\'sLicenseNo')}}">
                     @else
                         <input type="text" class="form-control" id="driver\'sLicenseNo" name="driver\'sLicenseNo" placeholder="" value="{{$employee->license}}">
                     @endif
                    
                </div>
            </div>
            <div class="form-group">
                
                <label class="control-label col-sm-2" for="nationality">Nationality:</label>
                <div class="col-sm-2">
                     @if(old('nationality')!=null)
                        <input type="text" class="form-control" id="nationality" name="nationality"  value="{{old('nationality')}}">
                     @else
                         <input type="text" class="form-control" id="nationality" name="nationality" placeholder="" value="{{$employee->nationality}}">
                     @endif
                    
                </div>
                
            </div>
          
            
            <hr  align=center size=2>
            <div class="form-group">
                <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
               
            @else 
               
            @endif
       </form>    
        <script type="text/javascript">
            $(function () {
                $('#dateOfBirth').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        </script>
    
    </div>
  </div>
 


    
@endsection
