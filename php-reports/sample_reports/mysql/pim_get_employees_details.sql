-- Get Employees Details
-- This report lets you view employees' details.
-- 



SELECT e.emp_id AS 'Employee id',
		first_name AS'First Name',
        last_name AS 'Last Name',
        nic AS 'NIC no',
        work_no AS'Conatct No',
        work_email AS 'Email',
        d.name AS 'Department',
        goc.name AS 'Group of Companies',
        jc.category_name AS 'Job Category',
        es.title AS 'Employment Status'
        
FROM `th_hr_employees` e
	LEFT JOIN `th_hr_employee_contact_details` cd ON e.id=cd.emp_id 
	LEFT JOIN `th_hr_employee_employment_details` ed ON e.id = ed.emp_id
    LEFT JOIN `th_sys_departments` d ON ed.department=d.id
    LEFT JOIN `th_sys_group_of_companies` goc ON ed.group_of_companies=goc.id
    LEFT JOIN `th_sys_job_categories` jc ON ed.job_category=jc.id
    LEFT JOIN `th_sys_employment_statuses` es ON ed.employment_status=es.id
