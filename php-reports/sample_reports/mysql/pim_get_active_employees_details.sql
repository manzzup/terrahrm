-- Active Employees' Details
-- This report lets you view all Active employees.
--



SELECT
	IF(ed.hr_active_flag='0',"Active","Inactive") AS 'Employee Status',
		e.emp_id AS 'Employee id',
		first_name AS'First Name',
        last_name AS 'Last Name',
        joined_date AS 'Joined Date',
        goc.name AS 'Group of Companies',
     	d.name AS 'Department',
        
        jc.category_name AS 'Job Category',
        es.title AS 'Employment Status',
        left_date AS'Left Date'
        
FROM `th_hr_employees` e
	LEFT JOIN `th_hr_employee_contact_details` cd ON e.id=cd.emp_id 
	LEFT JOIN `th_hr_employee_employment_details` ed ON e.id = ed.emp_id
    LEFT JOIN `th_sys_departments` d ON ed.department=d.id
    LEFT JOIN `th_sys_group_of_companies` goc ON ed.group_of_companies=goc.id
    LEFT JOIN `th_sys_job_categories` jc ON ed.job_category=jc.id
    LEFT JOIN `th_sys_employment_statuses` es ON ed.employment_status=es.id
WHERE ed.hr_active_flag='0'