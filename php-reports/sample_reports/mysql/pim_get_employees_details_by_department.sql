-- Employees' Details by Department
-- This report lets you view all employees in a specified department.
-- You can enter a department to drill down into all employees' details for that department.
-- VARIABLE: { 
--      name: 'department', 
--      display: 'Department',
--      type: 'text',
--
-- }


SELECT  e.emp_id AS 'Employee id',
        first_name AS'First Name',
        last_name AS 'Last Name',
        
       
        IF(ed.hr_active_flag='0',"Active","Inactive") AS 'Employee Status',
        d.name AS 'Department',
        goc.name AS 'Group of Companies',
        jc.category_name AS 'Job Category',
        es.title AS 'Employment Status'
        
FROM `th_hr_employees` e
	LEFT JOIN `th_hr_employee_contact_details` cd ON e.id=cd.emp_id 
	LEFT JOIN `th_hr_employee_employment_details` ed ON e.id = ed.emp_id
    LEFT JOIN `th_sys_departments` d ON ed.department=d.id
    LEFT JOIN `th_sys_group_of_companies` goc ON ed.group_of_companies=goc.id
    LEFT JOIN `th_sys_job_categories` jc ON ed.job_category=jc.id
    LEFT JOIN `th_sys_employment_statuses` es ON ed.employment_status=es.id
WHERE d.name="{{department}}"