-- Leave Summary by Department
-- This report lets you view leave summary of employees in a given department.
-- VARIABLE: { 
--      name: 'department', 
--      display: 'Department',
--      type: 'select',
--	database_options : {
--				table: "th_sys_departments",
--				column: "name"
--			}
--
-- }


SELECT  la.emp_id AS 'Employee ID',
        CONCAT_WS(' ',e.first_name,NULL,e.last_name) AS 'Full Name',
        d.name AS 'Department',
        CASE ai.status 
        	WHEN 2
            THEN 'No Check-in'
            WHEN 4
            THEN 'Half Day'
        
        END AS 'Issue',
        
        ah.date AS 'Issue Date',
        
        CASE ai.resolve
        	WHEN 1
            THEN 'Yes'
            WHEN 0
            THEN 'No'
        END AS 'Leave Applied?',
        
        CASE ai.resolve
        	WHEN 1
            THEN la.created_at
            WHEN 0
            THEN '-'
        END AS 'Applied Date and Time'
        



FROM `th_hr_leave_applications` la
	LEFT JOIN `th_hr_employees` e ON la.emp_id=e.id
	LEFT JOIN `th_hr_attendance_histories` ah ON la.emp_id=ah.emp_id
    LEFT JOIN `th_hr_attendance_issues` ai ON ai.attendance_history_id=ah.id
    LEFT JOIN `th_hr_employee_employment_details` ed ON la.emp_id=ed.emp_id
    LEFT JOIN `th_sys_departments` d ON ed.department = d.id
    WHERE d.name="{{department}}"
