-- Leave Summary by Employee
-- This report lets you view leave summary of an employee.
-- VARIABLE: { 
--	name:'emp_id',
--	display:'Employee ID',
--	type:'text'
-- }


SELECT e.emp_id AS 'Employee ID',
        CONCAT_WS(' ',e.first_name,NULL,e.last_name) AS 'Full Name',       
        CASE ai.status 
        	WHEN 2
            THEN 'No Check-in'
            WHEN 4
            THEN 'Half Day'
        
        END AS 'Issue',
        
        ah.date AS 'Issue Date',
        
        CASE ai.resolve
        	WHEN 1
            THEN 'Yes'
            WHEN 0
            THEN 'No'
        END AS 'Leave Applied?',
        
        CASE ai.resolve
        	WHEN 1
            THEN la.created_at
            WHEN 0
            THEN '-'
        END AS 'Applied Date'
        



FROM `th_hr_leave_applications` la
	LEFT JOIN `th_hr_employees` e ON la.emp_id=e.id
	LEFT JOIN `th_hr_attendance_histories` ah ON la.emp_id=ah.emp_id
    LEFT JOIN `th_hr_attendance_issues` ai ON ai.attendance_history_id=ah.id
    LEFT JOIN `th_hr_employee_employment_details` ed ON la.emp_id=ed.emp_id
    LEFT JOIN `th_sys_departments` d ON ed.department = d.id
WHERE e.emp_id LIKE '%{{emp_id}}%'
