-- Attendance Summary Report
-- This report lets you view attendance summary of employees within a specified time period.
-- VARIABLE: { 
--name:'startDate',
--display:'Start Date',
--type:'date'
-- }


SELECT  e.emp_id AS 'Employee ID',
		CONCAT_WS(' ', e.first_name,NULL, e.last_name) AS 'Full Name',
		DATEDIFF((NOW()-1),CAST('{{startDate}}' AS DATE)) AS 'WorkDay Count',
		CASE 
			WHEN COUNT(att_his.check_in_time) > COUNT(att_his.check_out_time)
				THEN COUNT(att_his.check_in_time) 
			ELSE COUNT(att_his.check_out_time) 
		END AS 'PresentDay Count',
		DATEDIFF((NOW()-1),CAST('{{startDate}}' AS DATE))- COUNT(att_his.check_in_time) AS 'AbsentDay Count',
		SUM(att_his.over_time_minutes) AS 'O/T minutes',
		SUM(att_his.over_time_hours) AS 'O/T hours',
		SUM(att_his.early_minutes) AS 'Early minutes',
		SUM(att_his.late_minutes) AS 'Late minutes',
		SUM(att_his.work_time_hours) AS 'Work time hours',
		CASE 
			WHEN COUNT(att_his.check_in_time) > COUNT(att_his.check_out_time)
				THEN (((COUNT(att_his.check_in_time))/DATEDIFF('2016-06-30',CAST('2016-01-01' AS DATE)))*100)
			ELSE (((COUNT(att_his.check_out_time))/DATEDIFF('2016-06-30',CAST('2016-01-01' AS DATE)))*100)
		END AS 'Attendance Rate'

FROM `th_hr_attendance_histories` att_his
	LEFT JOIN `th_hr_employees` e ON att_his.emp_id=e.id
    
    
WHERE att_his.date BETWEEN CAST('{{startDate}}' AS DATE) AND (NOW()-1)
GROUP BY e.emp_id

